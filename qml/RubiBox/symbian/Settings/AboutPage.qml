import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../Component"
import "../Delegates"
import "../Buttons"

DevPDA{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            iconSource: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }


    devText: qsTr("About")
    loadingVisible: false


    Image{
        id: devpdaLogo
        z: -1000
        anchors {right: parent.right; bottom: parent.bottom; margins: platformStyle.paddingLarge}
        smooth: true
        source: "../Images/devpda.png"
        width: sourceSize.width
        height: sourceSize.height
        sourceSize.width: window.inPortrait ? parent.width/2 : parent.height/2
        sourceSize.height: window.inPortrait ? parent.width/2 : parent.height/2
        opacity: 0.6
    }


    Flickable{
        id: flicker
        anchors {
            fill: parent
            topMargin: devMargin
            margins: platformStyle.paddingLarge
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            spacing: 10
            width: parent.width


            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Info")
            }

            Row{
                spacing: 5
                width: parent.width

                Image{
                    id: imageLogo
                    sourceSize.width: 100
                    sourceSize.height: 100
                    width: sourceSize.width
                    height: sourceSize.height
                    smooth: true
                    source: "../Images/logo.png"
                }

                Column{
                    spacing: 5
                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                    width: parent.width - imageLogo.width - platformStyle.paddingLarge

                    Label{
                        platformInverted: Settings.inverted
                        font.pixelSize: platformStyle.fontSizeMedium
                        text: qsTr("RubiBox v%1").arg(version)
                        font.bold: true
                    }

                    Label{
                        platformInverted: Settings.inverted
                        font.pixelSize: platformStyle.fontSizeSmall
                        font.italic: true
                        text: qsTr("A fully-featured Box.com client")
                    }
                }
            }
            Label{
                platformInverted: Settings.inverted
                text: qsTr("Copyright (c) 2010-2013 DevPDA<br/><a style='color:%1' href='http://devpda.net'>www.devpda.net</a>").arg(Settings.activeColor)
                font.pixelSize: platformStyle.fontSizeSmall
                width: parent.width
                onLinkActivated: Qt.openUrlExternally(link)
            }


            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Thanks to")
            }


            Label{
                platformInverted: Settings.inverted
                width: parent.width
                text: qsTr("<b>Matias Perez</b> - %1").arg("<font size=\"2\">for various icon</font>")
            }

            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Description")
            }

            Label{
                platformInverted: Settings.inverted
                id: descriptionLabel
                wrapMode: Text.Wrap
                text: qsTr("RubiBox is a mobile application for Nokia devices based on Qt and QML framework, built on Box.com application programming interface.")
                width: parent.width
            }

            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Privacy policy")
            }

            Label{
                id: policy
                platformInverted: Settings.inverted
                wrapMode: Text.Wrap
                width: parent.width
                text: qsTr("This privacy policy sets out how RubiBox uses and protects any information that you enter in the application when you use it.")+"<br/>"+
                      "<br/><em>"+qsTr("What information is stored")+"</em><br\><br\>"+
                      qsTr("RubiBox store only access token. These are stored when you grant access to your Box.com account.")+
                      "<br/><br/><em>"+qsTr("What we do with the information")+"</em><br\><br\>"+
                      qsTr("The access token are stored locally on your phone are not transmitted or distributed to anyone. They are protected by the phone security system and are deleted when you remove RubiBox from your phone.")
            }
        }
    }
}
