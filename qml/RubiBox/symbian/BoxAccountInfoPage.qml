// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "Component"
import "Delegates"
import "Buttons"
import "../scripts/createobject.js" as ObjectCreator

DevPDA{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            iconSource: "toolbar-previous"
            onClicked: {
                root.pageStack.pop()
            }
        }
        ToolIcon{
            iconSource: "toolbar-refresh"
            onClicked: {
                canShow = false
                Session.userInfo()
            }
        }
    }

    property bool canShow: false
    devTextTop: Database.active_account
    devText: qsTr("Account info")
    onStatusChanged: {
        if (status === PageStatus.Active){
            Session.userInfo()
        }
    }


    Connections{
        target: Box
        onUserInfoDone: {
            accountLabel.text = user.name
            loginLabel.text = user.login
            var baseDateTimeCreated = user.created_at.split("T")
            var dateCreated = baseDateTimeCreated[0]
            var timeCreated = baseDateTimeCreated[1].split("-")[0]
            var baseDateTimeModified = user.modified_at.split("T")
            var dateModified = baseDateTimeModified[0]
            var timeModified = baseDateTimeModified[1].split("-")[0]
            createdLabel.text = dateCreated + " | " + timeCreated
            modifiedLabel.text = dateModified + " | " + timeModified
            statusLabel.text = (user.status === "active") ? "<font color=\"green\">" + user.status : "<font color=\"red\">" + user.status
            jobLabel.text = user.job_title
            phoneLabel.text = user.phone
            addressLabel.text = user.address
            spaceUsedLabel.text = Helper.convSize(user.space_used)
            spaceTotalLabel.text = Helper.convSize(user.space_amount)
            maxSize.text = Helper.convSize(user.max_upload_size)
            progressBar.value = (user.space_used*100)/user.space_amount
            canShow = true
        }
    }


    Flickable{
        id: flick
        anchors {
            fill: parent
            topMargin: devMargin
            margins: platformStyle.paddingMedium
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick
        opacity: canShow ? 1 : 0


        Behavior on opacity {PropertyAnimation{duration: !canShow ? 100 : 300}}


        Column{
            id: content
            spacing: 10
            anchors{
                left: parent.left
                right: parent.right
            }


            Separator{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Basic info")
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - statusLabel.width
                    text: qsTr("Status:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    platformInverted: Settings.inverted
                    id: statusLabel
                    font.bold: true
                }
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - accountLabel.width
                    text: qsTr("Name:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: accountLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - loginLabel.width
                    text: qsTr("Login:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: loginLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            Row{
                visible: jobLabel.text !== ""
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - jobLabel.width
                    text: qsTr("Job:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: jobLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            Row{
                visible: phoneLabel.text !== ""
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - phoneLabel.width
                    text: qsTr("Phone:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: phoneLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            Row{
                visible: addressLabel.text !== ""
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    id: addressWdt
                    width: parent.width - addressLabel.width
                    text: qsTr("Address:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: addressLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - createdLabel.width
                    text: qsTr("Created at:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: createdLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - modifiedLabel.width
                    text: qsTr("Modified at:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: modifiedLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }

            Separator{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Space info")
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - maxSize.width
                    text: qsTr("Max. upload size:")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: maxSize
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }

            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    width: parent.width - spaceWdt.width
                    text: qsTr("Used")
                    color: platformStyle.colorNormalMid

                }
                Label{
                    id: spaceWdt
                    text: qsTr("Total")
                    color: platformStyle.colorNormalMid
                }
            }
            Row{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Label{
                    id: spaceUsedLabel
                    width: parent.width - spaceTotalLabel.width
                    platformInverted: Settings.inverted
                    font.bold: true
                }
                Label{
                    id: spaceTotalLabel
                    platformInverted: Settings.inverted
                    font.bold: true
                }
            }
            ProgressBar{
                id: progressBar
                anchors{
                    left: parent.left
                    right: parent.right
                }
                minimumValue: 0
                maximumValue: 100
                platformInverted: Settings.inverted
            }

        }
    }
}
