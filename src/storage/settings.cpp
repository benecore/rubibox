#include "settings.h"

Settings::Settings(QObject *parent) :
    QObject(parent)
{
#ifdef Q_WS_SIMULATOR
#ifdef QT_DEBUG
    qDebug() << "Simulator nastavenie";
#endif
    settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, QString("RubiBox"), QString("settings"));
#elif defined(Q_OS_SYMBIAN)
    #ifdef QT_DEBUG
    qDebug() << "Symbian nastavenie";
#endif
    settings = new QSettings(QSettings::IniFormat, QSettings::SystemScope, QString("/"), QString("settings"));
#else
    #ifdef QT_DEBUG
    qDebug() << "MeeGo nastavenie";
#endif
    settings = new QSettings(QSettings::UserScope, QString("RubiBox"), QString("settings"));
#endif

    settings->beginGroup(APPEARANCE);
    m_activeColor = settings->value("activeColor", "#056abe").toString();
    m_activeColorString = settings->value("activeColorString", "color8-").toString();
    m_activeColorScheme = settings->value("activeColorScheme", "8").toString();
    m_inverted = settings->value("inverted", false).toBool();
    m_nativeIcons = settings->value("native_icons", false).toBool();
    m_colorizeHLine = settings->value("colorize_h_line", false).toBool();
    settings->endGroup();
    settings->beginGroup(OPTIONS);
    m_multiSelection = settings->value("multi_selection", false).toBool();
    settings->endGroup();
    settings->beginGroup(BASE);
    m_maxUpSize = settings->value("max_up_size", 262144000).toLongLong();
    m_orientation = settings->value("orientation", 0/* Auto orientation default*/).toInt();
    m_sectionSeparator = settings->value("section_separator", false).toBool();
    m_itemsLimit = settings->value("itemsLimit", 100).toInt();
#ifdef Q_OS_HARMATTAN
    m_downloadFolder = settings->value("download_folder", "/home/user/MyDocs/RubiBox").toString();
#else
    m_downloadFolder = settings->value("download_folder", "E:/RubiBox").toString();
#endif
    settings->endGroup();

#ifdef QT_DEBUG
    qDebug() << "STORAGE LOC: " << QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif
}

Settings::~Settings()
{
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO << " DESTRUCTOR";
#endif
    delete settings;
}
