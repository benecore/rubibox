// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../common"
import "../../scripts/const.js" as Const
import "../../scripts/utils.js" as Utils


Sheet{
    id: root

    property alias placeHolder: field.placeholderText

    signal okClicked(string name);

    rejectButtonText: qsTr("Cancel")
    acceptButtonText: qsTr("Continue")
    platformStyle: SheetStyle{
        background: appStyle.background
    }
    onAccepted: {
        root.okClicked(field.text)
    }
    onRejected: {Session.action = "normal"; root.destroy(600)}
    onStatusChanged: {
        if (status == DialogStatus.Opening){
            field.focus = true
        }
    }


    content: Item{
        anchors.fill: parent
        anchors.margins: Const.PADDING_LARGE

        Column{
            width: parent.width
            spacing: 10

            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Const.FONT_LARGE
                text: qsTr("Copy/Move Conflict!")
                color: !Settings.inverted ? "black" : "white"
            }

            Label{
                width: parent.width
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Const.FONT_SMALL
                color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
                text: Utils.CM_ITEM_NAME
            }

            HLine{
                width: parent.width
            }

            TextField{
                id: field
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width-(Const.PADDING_LARGE*2)
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                echoMode: TextInput.Normal
                validator: RegExpValidator{regExp: /[^\\//|:?*<>]*/}
                text: Utils.CM_ITEM_NAME
                Keys.onReturnPressed: {
                    if (text !== ""){
                        focus = false;
                        platformCloseSoftwareInputPanel()
                        root.accept()
                    }else{
                        infoBanner.parent = root
                        infoBanner.text = qsTr("New name can't be empty")
                        infoBanner.show()
                    }
                }
                onFocusChanged: focus ? platformOpenSoftwareInputPanel() : platformCloseSoftwareInputPanel()
            }
        }
    }
}
