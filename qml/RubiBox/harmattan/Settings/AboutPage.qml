import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../Component"
import "../Delegates"
import "../Buttons"
import "../../scripts/const.js" as Const
import "../../scripts/createobject.js" as ObjectCreator

DevPDA{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }


    devText: qsTr("About")
    loadingVisible: false


    Image{
        id: devpdaLogo
        z: -1000
        anchors {right: parent.right; bottom: parent.bottom; margins: Const.PADDING_LARGE}
        smooth: true
        source: "../Images/devpda.png"
        width: sourceSize.width
        height: sourceSize.height
        sourceSize.width: window.inPortrait ? parent.width/2 : parent.height/2
        sourceSize.height: window.inPortrait ? parent.width/2 : parent.height/2
        opacity: 0.6
    }


    Flickable{
        id: flicker
        anchors {
            fill: parent
            topMargin: devMargin
            margins: Const.MARGIN_XLARGE
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            spacing: 10
            width: parent.width


            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Info")
            }

            Row{
                spacing: 5
                width: parent.width

                Image{
                    id: imageLogo
                    sourceSize.width: 100
                    sourceSize.height: 100
                    width: sourceSize.width
                    height: sourceSize.height
                    smooth: true
                    source: "../Images/logo.png"
                }

                Column{
                    spacing: 5
                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                    width: parent.width - imageLogo.width - Const.MARGIN_XLARGE

                    Label{
                        font.pixelSize: Const.FONT_DEFAULT
                        text: qsTr("RubiBox v%1").arg(version)
                        font.bold: true
                    }

                    Label{
                        font.pixelSize: Const.FONT_SMALL
                        font.italic: true
                        text: qsTr("A fully-featured Box.com client")
                    }
                }
            }
            Label{
                text: qsTr("Copyright (c) 2010-2013 DevPDA<br/><a style='color:%1' href='http://devpda.net'>www.devpda.net</a>").arg(Settings.activeColor)
                font.pixelSize: Const.FONT_LSMALL
                width: parent.width
                onLinkActivated: Qt.openUrlExternally(link)
            }


            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Thanks to")
            }


            Label{
                width: parent.width
                text: qsTr("<b>Matias Perez</b> - %1").arg("<font size=\"2\">for various icon</font>")
            }

            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Description")
            }

            Label{
                id: descriptionLabel
                wrapMode: Text.Wrap
                text: qsTr("RubiBox is a mobile application for Nokia devices based on Qt and QML framework, built on Box.com application programming interface.")
                width: parent.width
            }

            Separator{
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Privacy policy")
            }

            Label{
                id: policy
                wrapMode: Text.Wrap
                width: parent.width
                text: qsTr("This privacy policy sets out how RubiBox uses and protects any information that you enter in the application when you use it.")+"<br/>"+
                      "<br/><em>"+qsTr("What information is stored")+"</em><br\><br\>"+
                      qsTr("RubiBox store only access token. These are stored when you grant access to your Box.com account.")+
                      "<br/><br/><em>"+qsTr("What we do with the information")+"</em><br\><br\>"+
                      qsTr("The access token are stored locally on your phone are not transmitted or distributed to anyone. They are protected by the phone security system and are deleted when you remove RubiBox from your phone.")
            }
        }
    }
}
