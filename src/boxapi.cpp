#include "boxapi.h"

/*
static void printHeaders(QNetworkReply *reply)
{
    QNetworkRequest request = reply->request();
    qDebug() << "\n--------------------" << "Request headers" << "--------------------";
    QList<QByteArray> reqHeaders = request.rawHeaderList();
    foreach( QByteArray reqName, reqHeaders )
    {
        QByteArray reqValue = request.rawHeader(reqName);
        qDebug() << reqName << ": " << reqValue;
    }
    qDebug() << "\n--------------------" << "Reply headers" << "--------------------";
    QList<QByteArray> repHeaders = reply->rawHeaderList();
    foreach( QByteArray reqName, repHeaders )
    {
        QByteArray reqValue = reply->rawHeader(reqName);
        qDebug() << reqName << ": " << reqValue;
    }
}
*/

/**
  * Constructor
  */
BoxApi::BoxApi(QObject *parent) :
    QObject(parent), upFile(0), reply(0), m_prepareDownload(false)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(finished(QNetworkReply*)));
    connect(manager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
            SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));

    file_transfers = new QNetworkAccessManager(this);
    connect(file_transfers, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
            SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));

    //download = new Download(file_transfers);
    //connect(download, SIGNAL(transferDone(int)), SIGNAL(transferDone(int)));
    //connect(download, SIGNAL(transferDone()), SLOT(startNext()));
}



BoxApi::BoxApi(Settings *settings, QObject *parent) :
    QObject(parent), upFile(0), reply(0), m_settings(settings), m_prepareDownload(false)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(finished(QNetworkReply*)));
    connect(manager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
            SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));

    file_transfers = new QNetworkAccessManager(this);
    connect(file_transfers, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
            SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
}


/**
  * Destructor
  */
BoxApi::~BoxApi()
{
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO << " DESTRUCTOR";
#endif
    delete manager;
    delete file_transfers;
    delete upFile;
}


/**
  * Sets action for parsing reply inside the finished function
  * @param action Action enum
  * @remarks must be set before do request
  */
void BoxApi::setAction(Action action)
{
    m_action = action;
}


/**
  * Sets token for Auth 2.0 header
  * @param token Auth2.0 token
  * @remarks must be set before request
  */

void BoxApi::setToken(QByteArray token)
{
    m_token = token;
}


/**
  * Sets If-Match header (more info Box.com Api docs)
  * @param etag If-Match value
  */
void BoxApi::setIfMatch(const QByteArray etag)
{
    m_etag = etag;
}


/**
  * Sets action for parsing reply inside the finished function
  * @param index current item index
  * @remarks must be set before do remove request
  */
void BoxApi::setIndex(const int index)
{
    m_index = index;
}


/**
  * Clear m_token, m_etag
  * @remarks must be clear when doing new request
  */
void BoxApi::clearData()
{
    m_etag.clear();
    m_token.clear();
}


void BoxApi::removeFromQueue(const QVariant &id)
{
    foreach(ModelItem *item, m_queue){
        if (item->id() == id){
            m_queue.removeOne(item);
        }
    }
    emit removeFromQueueDone();
}


/**
  * getAuthToken
  * @remarks after retrieve Grant code
  */
void BoxApi::getAuthToken(const QVariant code)
{
    emit requestStart();
    QUrl url("https://www.box.com/api/oauth2/token");

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QUrl postParams;
    postParams.addQueryItem("grant_type", "authorization_code");
    postParams.addQueryItem("code", code.toString().toUtf8());
    postParams.addQueryItem("client_id", CLIENT_ID);
    postParams.addQueryItem("client_secret", CLIENT_SECRET);

#ifdef QT_DEBUG
    qDebug() << "-- getAuthToken --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: POST" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postParams.encodedQuery() << endl << \
                "-- getAuthToken --";
#endif

    manager->post(req, postParams.encodedQuery());
}


/**
  * refreshToken
  * @remarks refresh auth token
  */
void BoxApi::renewToken()
{
    emit requestStart();
    QUrl url("https://www.box.com/api/oauth2/token");

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QUrl postParams;
    postParams.addQueryItem("refresh_token", Database::instance()->getRefreshToken());
    postParams.addQueryItem("client_id", CLIENT_ID);
    postParams.addQueryItem("client_secret", CLIENT_SECRET);
    postParams.addQueryItem("grant_type", "refresh_token");

#ifdef QT_DEBUG
    qDebug() << "-- renewToken --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: POST" << endl << \
                "Request url: " << url.toString() << endl << \
                "-- renewToken --";
#endif

    manager->post(req, postParams.encodedQuery());
}


/**
  * getTicket
  * @remarks first login
  */
void BoxApi::getTicket()
{
    emit requestStart();
    QUrl url("https://www.box.com/api/1.0/rest");
    url.addQueryItem("action", "get_ticket");
    url.addQueryItem("api_key", /*API_KEY*/"");

    QNetworkRequest req(url);

#ifdef QT_DEBUG
    qDebug() << "-- getTicket --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: GET" << endl << \
                "Request url: " << url.toString() << endl << \
                "-- getTicket --";
#endif

    manager->get(req);
}


/**
  * List folder by ID
  * @param id folder id
  */
void BoxApi::listFolder(const QVariant id, const int offset)
{
    emit requestStart();
    if (offset == 0){
        emit clear();
    }
    QUrl url(QString("https://api.box.com/2.0/folders/%1?fields=size,sequence_id,etag,name,sha1,parent,path_collection,description,shared_link,item_collection,sync_state,permissions"\
                     "&limit=%2"\
                     "&offset=%3")
             .arg(id.toString())
             .arg(QString::number(m_settings->itemsLimit()))
             .arg(QString::number(offset)));

    QNetworkRequest req(url);
    setHeader(req);

#ifdef QT_DEBUG
    qDebug() << "-- List folder --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: GET" << endl << \
                "Request url: " << url.toString() << endl << \
                "-- List folder --";
#endif

    manager->get(req);
}


/**
  * Create folder
  * @param name name of new folder
  * @param id id where have been folder created
  */
void BoxApi::createFolder(const QVariant folderName, const QVariant folderId)
{
    emit requestStart();
    QUrl url("https://api.box.com/2.0/folders");

    QNetworkRequest req(url);
    setHeader(req);

    QByteArray postData = "{\"name\":";
    postData += "\""+folderName.toString().toUtf8()+"\",";
    postData += "\"parent\":{\"id\":\"" + folderId.toString().toUtf8() + "\"}}";

#ifdef QT_DEBUG
    qDebug() << "-- Create folder --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: POST" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postData << endl << \
                "-- Create folder --";
#endif

    manager->post(req, postData);
}


/**
  * Copy item
  * @param isFolder check if is folder
  * @param itemId id of item
  * @param destinationId Id where have been copied the item
  */
void BoxApi::copyItem(const bool isFolder,
                      const QVariant itemId,
                      const QVariant destinationId,
                      const QVariant newName)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/%1/%2/copy").arg(isFolder ? "folders" : "files").arg(itemId.toString()));

    QNetworkRequest req(url);
    setHeader(req);

    QByteArray postData;
    if (newName.isNull()){
        postData = "{\"parent\":";
        postData += "{\"id\":" + destinationId.toString().toUtf8() + "}}";
    }else{
        postData = "{\"name\":\"" + newName.toString().toUtf8() + "\", \"parent\":";
        postData += "{\"id\":" + destinationId.toString().toUtf8() + "}}";
    }

#ifdef QT_DEBUG
    qDebug() << "-- Copy item --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: POST" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postData << endl << \
                "-- Copy item --";
#endif

    manager->post(req, postData);

}

/**
  * Copy item
  * @param isFolder check if is folder
  * @param itemId id of item
  * @param destinationId Id where have been moved the item
  */
void BoxApi::moveItem(const bool isFolder,
                      const QVariant itemId,
                      const QVariant destinationId,
                      const QVariant newName)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/%1/%2").arg(isFolder ? "folders" : "files").arg(itemId.toString()));

    QNetworkRequest req(url);
    setHeader(req);

    QByteArray postData;
    if (newName.isNull()){
        postData = "{\"parent\":";
        postData += "{\"id\":" + destinationId.toString().toUtf8() + "}}";
    }else{
        postData = "{\"name\":\"" + newName.toString().toUtf8() + "\", \"parent\":";
        postData += "{\"id\":" + destinationId.toString().toUtf8() + "}}";
    }

#ifdef QT_DEBUG
    qDebug() << "-- Move item --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: PUT" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postData << endl << \
                "-- Move item --";
#endif

    manager->put(req, postData);
}



/**
  * Rename item
  * @param isFolder item is folder
  * @param newName new name of item
  * @param id current item id
  */
void BoxApi::renameItem(const bool isFolder,
                        const QVariant newName,
                        const QVariant id)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/%1/%2").arg(isFolder ? "folders" : "files").arg(id.toString()));

    QNetworkRequest req(url);
    setHeader(req);
    if (!m_etag.isEmpty())
        req.setRawHeader("If-Match", m_etag);

    QByteArray postData = "{\"name\":";
    postData += "\""+newName.toString().toUtf8()+"\"}";

#ifdef QT_DEBUG
    qDebug() << "-- Rename item --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: PUT" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postData << endl << endl;
#endif

    manager->put(req, postData);
}

/**
  * Remove item
  * @param isFolder if item is folder
  * @param id id of item which will been removed
  * @param recursive remove non-empty folder if is true
  */
void BoxApi::removeItem(const bool isFolder, const QVariant id, const QVariant recursive)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/%1/%2").arg(isFolder ? "folders" : "files").arg(id.toString()));
    url.addQueryItem("recursive", recursive.toString().toUtf8());

    QNetworkRequest req(url);
    setHeader(req);
    if (!m_etag.isEmpty())
        req.setRawHeader("If-Match", m_etag);

#ifdef QT_DEBUG
    qDebug() << "-- Remove item --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: DELETE" << endl << \
                "Request url: " << url.toString() << endl << \
                "-- Remove item --";
#endif

    manager->deleteResource(req);
}


/**
  * Change sync state
  * @param id folder id
  * @param sync_state new sync state
  */
void BoxApi::syncState(const QVariant id, const QVariant sync_state)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/folders/%2?fields=sync_state").arg(id.toString()));

    QNetworkRequest req(url);
    setHeader(req);
    if (!m_etag.isEmpty())
        req.setRawHeader("If-Match", m_etag);

    QByteArray postData = "{\"sync_state\":";
    postData += "\""+sync_state.toString().toUtf8()+"\"}";

#ifdef QT_DEBUG
    qDebug() << "-- Sync state --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: P" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postData << endl << \
                "-- Sync state --";
#endif

    manager->put(req, postData);
}


/**
  * userInfo
  * @remarks get user info
  */
void BoxApi::userInfo()
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/users/me"));

    QNetworkRequest req(url);
    setHeader(req);

#ifdef QT_DEBUG
    qDebug() << "-- User info --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: GET" << endl << \
                "Request url: " << url.toString() << endl << \
                "-- User info --";
#endif
    manager->get(req);
}


/**
  * userInfo
  * @param id file id
  * @remarks get file comments
  */
void BoxApi::comments(const QVariant id)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/files/%1/comments").arg(id.toString()));

    QNetworkRequest req(url);
    setHeader(req);
#ifdef QT_DEBUG
    qDebug() << "-- File comments --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: GET" << endl << \
                "Request url: " << url.toString() << endl << \
                "-- File comments --";
#endif
    manager->get(req);

}


void BoxApi::share_link(const bool isFolder, const QVariant id)
{
    emit requestStart();
    QUrl url(QString("https://api.box.com/2.0/%1/%2").arg(isFolder ? "folders" : "files").arg(id.toString()));

    QNetworkRequest req(url);
    setHeader(req);

    QByteArray postData = "{\"shared_link\":";
    postData += "{\"access\": \"open\"}}";

#ifdef QT_DEBUG
    qDebug() << "-- Share link --" << endl << \
                "Function: " << Q_FUNC_INFO << endl << \
                "Request type: PUT" << endl << \
                "Request url: " << url.toString() << endl << \
                "Data: " << postData << endl << \
                "-- File comments --";
#endif
    manager->put(req, postData);
}


/**
  * finished
  * @param reply pointer to the reply that has finished
  * @remarks slot for finished request of QNetworkAccessManager
  */
void BoxApi::finished(QNetworkReply *reply)
{
    // Check if request finished without error
    if (reply->error() == QNetworkReply::NoError){
        // Switch statement for result parsing
        switch(m_action){
        case GET_TOKEN:
            parseToken(reply->readAll());
            break;
        case REFRESH_TOKEN:
            parseToken(reply->readAll());
            break;
        case GET_TICKET:
            parseTicket(reply->readAll());
            break;
        case LIST_FOLDER:
            parseItems(reply->readAll());
            break;
        case CREATE_FOLDER:
            parseCreateFolder(reply->readAll());
            break;
        case RENAME:
            parseRename(reply->readAll());
            break;
        case REMOVE:
            parseRemove(reply->readAll());
            break;
        case SYNC_STATE:
            parseSyncState(reply->readAll());
            break;
        case COPY:
            parseCopy(reply->readAll());
            break;
        case MOVE:
            parseMove(reply->readAll());
            break;
        case USER_INFO:
            parseUserInfo(reply->readAll());
            break;
        case COMMENTS:
            parseComments(reply->readAll());
            break;
        case SHARE_LINK:
            parseShareLink(reply->readAll());
            break;
        default:
            break;
        }
    }else{
        int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
        qDebug() << "ERROR CODE:" << errorCode;
        qDebug() << "ERROR RESPONSE:" << reply->errorString();
        qDebug() << "ERROR RESPONSE:" << reply->readAll();
        if (m_action == REFRESH_TOKEN && errorCode == 401){
            emit refreshTokenExpired();
        }
        emit requestFinishedWithError(errorCode);
    }
    reply->deleteLater();
}


/**
  * parseToken
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseToken(QByteArray response)
{
    qDebug() << "AUTH TOKEN: " << response;
    if (reader.parse(response)){
        QVariantMap result = reader.result().toMap();
        if (m_action == GET_TOKEN){
            emit tokenDone(result.value("access_token").toString(), result.value("refresh_token").toString());
            emit requestFinished();
        }else{
            emit refreshTokenDone(result.value("access_token").toString(), result.value("refresh_token").toString());
        }
    }else{
        qWarning() << "Unable to parse getAuthToken response";
    }

}


/**
  * parseTicket
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseTicket(QByteArray response)
{
    QString ticket;
    QXmlStreamReader reader(response);
    while(!reader.atEnd())
    {
        reader.readNext();
        if(reader.isStartElement() && reader.name() == "ticket")
        {
            ticket = reader.readElementText();
        }
    }
    emit ticketDone(ticket);
}


/**
  * parseItems
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseItems(QByteArray response)
{
    qDebug() << response;
    QString headerString("/");
    reader.parse(response);
    QVariantList itemList = reader.result().toMap()["item_collection"].toMap()["entries"].toList();
    foreach (QVariant temp, itemList){
        emit addItemDone(new BoxItem(temp.toMap()));
    }
    if (!itemList.count()){
        emit allItemsLoaded();
    }
    if (reader.result().toMap()["item_collection"].toMap()["offset"].toInt() > 0){
        return;
    }
    foreach(QVariant headerLabel, reader.result().toMap()["path_collection"].toMap()["entries"].toList()){
        if (headerLabel.toMap()["name"].toString() != "All Files"){
            headerString += headerLabel.toMap()["name"].toString() + "/";
        }
    }
    if (reader.result().toMap()["name"].toString() != "All Files")
        headerString += reader.result().toMap()["name"].toString();
    emit parentFolder(reader.result().toMap()["parent"].toMap()["id"]);
    emit currentFolder(reader.result().toMap()["id"]);
    QString headerTop = headerString.mid(headerString.lastIndexOf("/")+1);
    //QString headBottom = headerString.remove(headerTop);
    QStringList test = headerString.split("/");
    test.removeLast();
    QString headBottom = test.join("/");
    if (headBottom.isEmpty()){
        headBottom += "/";
    }
    if (headBottom.size() > 1){
        headBottom.append("/");
    }
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO << endl << "HEADER BOTTOM: " << headBottom << endl\
             << "HEADER TOP: " << headerTop;
#endif
    emit header(headBottom, headerTop);
    SortModel::instance()->reSort();
    emit requestFinished();
    emit itemsDone();
}


/**
  * parseCreateFolder
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseCreateFolder(QByteArray response)
{
    reader.parse(response);
    emit createFolderDone(0, new BoxItem(reader.result().toMap()));
    SortModel::instance()->reSort();
    emit requestFinished();
}


/**
  * parseCopy
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseCopy(QByteArray response)
{
    reader.parse(response);
    emit copyItemDone(0, new BoxItem(reader.result().toMap()));
    emit copyActionDone();
    SortModel::instance()->reSort();
    emit requestFinished();
}


/**
  * parseMove
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseMove(QByteArray response)
{
    reader.parse(response);
    emit moveItemDone(0, new BoxItem(reader.result().toMap()));
    emit moveActionDone();
    SortModel::instance()->reSort();
    emit requestFinished();
}



/**
  * parseRename
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseRename(QByteArray response)
{
    reader.parse(response);
    emit renameItemDone(QVariant("name"), reader.result().toMap()["name"]);
    SortModel::instance()->reSort();
    emit requestFinished();
}


/**
  * parseRename
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseRemove(QByteArray response)
{
    Q_UNUSED(response)
    // Remove response is empty so just emit the remove signal
    emit removeItemDone(m_index);
    SortModel::instance()->reSort();
    emit requestFinished();
}


void BoxApi::parseSyncState(QByteArray response)
{
    reader.parse(response);
    qDebug() << "Resposne" << response;
    emit syncStateDone(QVariant("sync_state"), reader.result().toMap()["sync_state"]);
    emit requestFinished();
}


/**
  * parseUserInfo
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseUserInfo(QByteArray response)
{
    reader.parse(response);
    QVariantMap res = reader.result().toMap();
    m_settings->setMaxUpSize(res["max_upload_size"]);
    qDebug() << "MAX_UP_SIZE: " << res["max_upload_size"];
    emit userInfoDone(res);
    emit requestFinished();
}


/**
  * parseComments
  * @param response QByteArray of QNetworkReply readAll
  */
void BoxApi::parseComments(QByteArray response)
{
    reader.parse(response);
    foreach (QVariant temp, reader.result().toMap()["entries"].toList()){
        emit commentsDone(temp.toMap());
    }
    emit requestFinished();

}


void BoxApi::parseShareLink(QByteArray response)
{
    reader.parse(response);
    QString shareLink = reader.result().toMap()["shared_link"].toMap()["url"].toString();
    qDebug() << shareLink;
    emit shareLinkDone(shareLink);
    emit requestFinished();
}


void BoxApi::sslErrors(QNetworkReply *reply, QList<QSslError> errors)
{
    reply->ignoreSslErrors(errors);
}


void BoxApi::addToTransferQueue(ModelItem *item)
{
    if (m_queue.isEmpty() && !output.isOpen() && !m_prepareDownload && !upFile){
        QTimer::singleShot(0, this, SLOT(startNext()));
    }
    m_queue.enqueue(item);
}


void BoxApi::cancelDownload()
{
    reply->abort();
    output.remove();
    reply = 0;
    startNext();
}

QVariant BoxApi::getCurrentItemId() const
{
    return m_item->toFolder();
}


void BoxApi::startNext()
{
    if (m_queue.isEmpty()){
#ifdef QT_DEBUG
        qDebug() << "All files downloaded";
#endif
        reply->deleteLater();
        emit allTransfersDone();
        return;
    }
    m_item = (TransferItem*)m_queue.dequeue();
    if (m_item->downloading()){
        prepareDownload();
    }else{
        startUpload();
    }
}



void BoxApi::prepareDownload()
{
    m_prepareDownload = true;
    m_item->setActive(true);
    QUrl url(QString("https://api.box.com/2.0/files/%1/content").arg(m_item->id().toString()));

    QNetworkRequest req(url);
    setHeader(req);
    connect(file_transfers, SIGNAL(finished(QNetworkReply*)),
            SLOT(prepareFinished(QNetworkReply*)));
    file_transfers->get(req);
}



void BoxApi::startUpload()
{
    disconnect(file_transfers, 0, 0, 0);

    m_item->setActive(true);
    QUrl url("https://upload.box.com/api/2.0/files/content");

    QNetworkRequest req(url);
    setHeader(req);

    req.setHeader(QNetworkRequest::ContentTypeHeader, "multipart/form-data; boundary=" + BOUNDARY);

    QByteArray data(SEPARATOR.toAscii());
    data.append("Content-Disposition: form-data; name=\"filename\";");
    data.append(QString("filename=\"%1\"%2").arg(m_item->name().toString()).arg(CRLF).toAscii()); // Filename
    data.append("Content-Type: application/octet-stream");
    data.append(QString("%1").arg(CRLF+CRLF).toAscii());
    QByteArray tail;
    tail.append(QString("%1%2").arg(CRLF).arg(SEPARATOR));
    tail.append("Content-Disposition: form-data; name=\"parent_id\"");
    tail.append(CRLF+CRLF);
    tail.append(m_item->toFolder().toString().toUtf8()); // Folder id
    tail.append(END_MULTI_FORM.toAscii());



    upFile = new QUpFile(m_item->path().toString(), data, tail, this);
    bool open = upFile->openFile();
#ifdef QT_DEBUG
    qDebug() << "FILE OPENED: " << open;
#endif
    if (!open){
        qDebug() << "FILE NOT OPENED";
        emit transferDone();
        startNext();
        return;
    }

    reply = file_transfers->post(req, upFile);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(uploadProgress(qint64,qint64)),
            SLOT(progress(qint64,qint64)));
    connect(reply, SIGNAL(finished()),
            SLOT(finishedUpload()));
    downloadTime.start();
}


void BoxApi::prepareFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError){
        QUrl location = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        if (!location.isEmpty()){
            qDebug() << "LOCATION: " << location.toString();
            doDownload(location);
            m_prepareDownload = false;
        }
    }else{
        int errorCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute ).toInt();
#ifdef QT_DEBUG
        qDebug() << "ERROR CODE:" << errorCode;
        qDebug() << "ERROR RESPONSE:" << reply->errorString();
        qDebug() << "ERROR RESPONSE:" << reply->readAll();
        qDebug() << "Download failed";
#endif
    }
}


void BoxApi::doDownload(const QUrl url)
{
    disconnect(file_transfers, 0, 0, 0);

    QNetworkRequest req(url);

    QDir dir(m_settings->downloadFolder());
    if (!dir.exists()){
        dir.mkpath(dir.path());
    }

#if defined(Q_OS_HARMATTAN) || defined(Q_OS_SYMBIAN)
    output.setFileName(QString("%1/%2").arg(m_settings->downloadFolder()).arg(m_item->name().toString()));
#else
    output.setFileName(QString("E:/RubiBox/%1").arg(m_item->name().toString()));
#endif
    if (!output.open(QIODevice::WriteOnly)) {
#ifdef QT_DEBUG
        qDebug() << "Problem opening file " << output.fileName() << " for download " << output.errorString();
#endif
        //emit transferDone();
        startNext();
        return; // skip this download
    }
    reply = file_transfers->get(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
            SLOT(progress(qint64,qint64)));
    connect(reply, SIGNAL(readyRead()),
            SLOT(ready()));
    connect(reply, SIGNAL(finished()),
            SLOT(finished()));
    downloadTime.start();
}


void BoxApi::progress(qint64 bytesReceived, qint64 bytesTotal)
{
    Q_UNUSED(bytesTotal)
    double speed = bytesReceived * 1000.0 / downloadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "b/s";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kb/s";
    } else {
        speed /= 1024*1024;
        unit = "Mb/s";
    }
    //emit received_size(bytesReceived);
    //emit speed(QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
    m_item->setReceivedSize(bytesReceived);
    m_item->setSpeed(QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
}


void BoxApi::ready()
{
    if (output.isOpen())
        output.write(reply->readAll());
}

void BoxApi::finished()
{
    output.close();
    if (reply->error() == QNetworkReply::NoError){
#ifdef QT_DEBUG
        //qDebug() << "File downloaded: " << m_item->name();
#endif
        m_item->setActive(false);
        emit transferDone();
        startNext();
    }
    else if (reply->error() == QNetworkReply::OperationCanceledError){
        emit transferDone();
    }
    else{
        int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
#ifdef QT_DEBUG
        qDebug() << "ERROR CODE:" << errorCode;
        qDebug() << "ERROR RESPONSE:" << reply->errorString();
        qDebug() << "ERROR RESPONSE:" << reply->readAll();
        qDebug() << "Download failed";
#endif
        m_item->setActive(false);
        emit transferDone();
        startNext();
    }
}



void BoxApi::finishedUpload()
{
    upFile->close();
    upFile = 0;
    if (reply->error() == QNetworkReply::NoError){
#ifdef QT_DEBUG
        //qDebug() << "File downloaded: " << m_item->name();
#endif
        reader.parse(reply->readAll());
        m_item->setActive(false);
        emit uploadItemDone(m_item->toFolder().toString(), reader.result().toMap()["entries"].toList().first().toMap());
        emit transferDone();
        startNext();
    }
    else if (reply->error() == QNetworkReply::OperationCanceledError){
        emit transferDone();
    }
    else{
        int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
#ifdef QT_DEBUG
        qDebug() << "ERROR CODE:" << errorCode;
        qDebug() << "ERROR RESPONSE:" << reply->errorString();
        qDebug() << "ERROR RESPONSE:" << reply->readAll();
        qDebug() << "Download failed";
#endif
        m_item->setActive(false);
        emit transferDone();
        startNext();
    }
}
