// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

ToolIcon{
    id: root

    signal clicked
    signal pressAndHold


    MouseArea{
        anchors.fill: parent
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress)
            root.pressAndHold()
        }
        onPressed: Helper.playEffect(Helper.ThemeBasicButton)
    }
}
