#include "session.h"

Session::Session(QObject *parent) :
    QObject(parent), m_current_folder_id("0"), m_parent_folder_id("0"), m_currentIndex(0), m_actionType("normal")
{
    db = Database::instance();
    settings = new Settings(this);
    boxApi = new BoxApi(settings, this);
    bModel = new Model(new BoxItem, this);
    bModelSort = SortModel::instance();
    bModelSort->setModel(bModel);
    helper = new Helper(this);
    tModel = new Model(new TransferItem, this);

    connect(boxApi, SIGNAL(requestStart()), SLOT(reqStart()));
    connect(boxApi, SIGNAL(requestFinished()), SLOT(reqFinish()));
    connect(boxApi, SIGNAL(header(QString,QString)), SLOT(setHeaderString(QString, QString)));
    connect(boxApi, SIGNAL(currentFolder(QVariant)), SLOT(setCurrentFolder(QVariant)));
    connect(boxApi, SIGNAL(parentFolder(QVariant)), SLOT(setParentFolder(QVariant)));

    /** Connect to BModel **/
    connect(boxApi, SIGNAL(clear()), bModelSort, SLOT(clear()));
    connect(boxApi, SIGNAL(addItemDone(ModelItem*)), bModelSort, SLOT(appendItem(ModelItem*)));
    connect(boxApi, SIGNAL(createFolderDone(int,ModelItem*)), bModelSort, SLOT(insertItem(int,ModelItem*)));
    connect(boxApi, SIGNAL(removeItemDone(int)), bModelSort, SLOT(removeItem(int)));
    connect(boxApi, SIGNAL(copyItemDone(int,ModelItem*)), bModelSort, SLOT(insertItem(int,ModelItem*)));
    connect(boxApi, SIGNAL(moveItemDone(int,ModelItem*)), bModelSort, SLOT(insertItem(int,ModelItem*)));


    connect(boxApi, SIGNAL(uploadItemDone(QString,QVariantMap)), this, SLOT(uploadItemDone(QString,QVariantMap)));
    connect(boxApi, SIGNAL(transferDone(int)), tModel, SLOT(removeItem(int)));

}

Session::~Session()
{
    delete boxApi;
    delete db;
    delete settings;
    delete bModel;
    delete tModel;
    delete helper;
    delete bModelSort;
}


void Session::setHeaderString(QString parent, QString current)
{
    m_header_parent = parent;
    m_header_current = current;
    emit headerChanged();
}


void Session::uploadItemDone(const QString toFolder, const QVariantMap item)
{
    if (toFolder == m_current_folder_id){
        if (!bModelSort->count())
            bModelSort->appendItem(new BoxItem(item));
        else
            bModelSort->insertItem(0, new BoxItem(item));
    }
}


void Session::prepareApi()
{
    boxApi->clearData();
    boxApi->setToken(db->getToken());
}


void Session::getAuthToken(const QVariant code)
{
    boxApi->clearData();
    boxApi->setAction(BoxApi::GET_TOKEN);

    boxApi->getAuthToken(code);
}


void Session::renewToken()
{
    boxApi->clearData();
    boxApi->setAction(BoxApi::REFRESH_TOKEN);

    boxApi->renewToken();
}


void Session::getTicket()
{
    boxApi->clearData();
    boxApi->setAction(BoxApi::GET_TICKET);

    boxApi->getTicket();
}


void Session::listFolder(const QVariant folderId, const int offset)
{
    prepareApi();
    boxApi->setAction(BoxApi::LIST_FOLDER);

    boxApi->listFolder(folderId, offset);
}


void Session::createFolder(const QVariant name)
{
    prepareApi();
    boxApi->setAction(BoxApi::CREATE_FOLDER);

    boxApi->createFolder(name, m_current_folder_id);
}



void Session::renameItem(const QVariant newName)
{
    BoxItem *item = (BoxItem*)bModelSort->getItem(m_currentIndex);
    prepareApi();
    boxApi->setAction(BoxApi::RENAME);
    boxApi->setIfMatch(item->etag().toString().toUtf8());

    boxApi->renameItem(item->isDir(), newName, item->id());
    connect(boxApi, SIGNAL(renameItemDone(QVariant,QVariant)), item, SLOT(setProperty(QVariant,QVariant)));
}


void Session::removeItem()
{
    BoxItem *item = (BoxItem*)bModelSort->getItem(m_currentIndex);
    prepareApi();
    boxApi->setAction(BoxApi::REMOVE);
    boxApi->setIfMatch(item->etag().toString().toUtf8());
    boxApi->setIndex(m_currentIndex);

    boxApi->removeItem(item->isDir(), item->id());

}


void Session::copyItem(const QVariant name)
{
    prepareApi();
    boxApi->setAction(BoxApi::COPY);

    boxApi->copyItem(_cmTemp.value("isDir").toBool(), _cmTemp.value("id"), m_current_folder_id, name);
}


void Session::moveItem(const QVariant name)
{
    prepareApi();
    boxApi->setAction(BoxApi::MOVE);

    boxApi->moveItem(_cmTemp.value("isDir").toBool(), _cmTemp.value("id"), m_current_folder_id, name);
}


void Session::syncState(const QVariant id, const QVariant sync_state)
{
    BoxItem *item = (BoxItem*)bModelSort->getItem(m_currentIndex);
    prepareApi();
    boxApi->setAction(BoxApi::SYNC_STATE);

    boxApi->syncState(id, sync_state);
    connect(boxApi, SIGNAL(syncStateDone(QVariant,QVariant)), item, SLOT(setProperty(QVariant,QVariant)));
}


void Session::userInfo()
{
    prepareApi();
    boxApi->setAction(BoxApi::USER_INFO);

    boxApi->userInfo();
}


void Session::share()
{
    BoxItem *item = (BoxItem*)bModelSort->getItem(m_currentIndex);
    prepareApi();
    boxApi->setAction(BoxApi::SHARE_LINK);

    boxApi->share_link(item->isDir(), item->id());
}


void Session::downloadItem(const QVariant id, const QVariant name, const QVariant size)
{
    prepareApi();
    if (!tModel->itemExists(id)){
        TransferItem *item = new TransferItem(id, name, size);
        tModel->appendItem(item);
        boxApi->addToTransferQueue(item);
    }else{
        qDebug() << "Existuje: " << name;
    }
}


void Session::uploadItem(const QString &path, const QString &size)
{
    prepareApi();
    //QFileInfo info(path.toLocalFile());
    TransferItem *item = new TransferItem(path, size);
    if (!tModel->itemExists(item->identi())){
        item->setToFolder(m_current_folder_id);
        tModel->appendItem(item);
        boxApi->addToTransferQueue(item);
        return;
    }
    delete item;
}


bool Session::isSelected() const
{
    for (int i = 0; i < bModelSort->rowCount(); ++i){
        BoxItem *item = (BoxItem*)bModelSort->getItem(i);
        if (item->selected()){
            return true;
        }
    }
    return false;
}


void Session::downloadSelected()
{
    for (int i = 0; i < bModelSort->rowCount(); ++i){
        BoxItem *item = (BoxItem*)bModelSort->getItem(i);
        if (item->selected()){
            downloadItem(item->id(), item->name(), item->size());
        }
    }
}
