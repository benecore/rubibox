#include "factory.h"

Factory::Factory(QObject *parent) :
    QObject(parent)
{
    myManager = new QNetworkAccessManager(this);
    connect(myManager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
            this, SLOT(ignoreSSLErrors(QNetworkReply*,QList<QSslError>)));
}



void Factory::ignoreSSLErrors(QNetworkReply *reply, QList<QSslError> errors)
{
    reply->ignoreSslErrors(errors);
}


QNetworkAccessManager *Factory::create(QObject *parent)
{
    Q_UNUSED(parent)
    return myManager;
}
