// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../scripts/utils.js" as Utils
import "../../common"


Item{
    id: root
    width: ListView.view.width

    height: nameLabel.height + sizeLabel.height + (platformStyle.paddingMedium*2)

    property bool cmCheck: (Session.action !== "normal")

    opacity: (!isDir && mouseArea.pressed) ? 0.8 : (cmCheck && !isDir || cmCheck && Utils.CM_ITEM_ID === id) ? 0.5 : 1
    enabled: opacity == 0.5 ? false : true


    scale: isDir && mouseArea.pressed ? 0.99 : 1

    signal clicked
    signal pressAndHold


    Image {
        id: indicator
        anchors{
            right: parent.right
            verticalCenter: parent.verticalCenter
            margins: platformStyle.paddingMedium
        }
        visible: isDir
        source: privateStyle.imagePath("qtg_graf_drill_down_indicator", Settings.inverted)
        sourceSize.width: platformStyle.graphicSizeSmall
        sourceSize.height: platformStyle.graphicSizeSmall
    }


    HLine{
        anchors.bottom: parent.bottom
    }

    Rectangle{
        anchors.fill: parent
        opacity: selected ? 1 : 0
        color: Settings.activeColor

        Behavior on opacity {PropertyAnimation{duration: selected ? 400 : 50}}
    }


    Image{
        id: thumb
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: platformStyle.paddingMedium
        }

        sourceSize.width: !Settings.nativeIcons ? platformStyle.graphicSizeLarge : 55
        sourceSize.height: !Settings.nativeIcons ? platformStyle.graphicSizeLarge : 55
        width: sourceSize.width
        height: sourceSize.height
        smooth: true
        source: Helper.getIcon(!Settings.inverted, isDir, 2, Settings.nativeIcons, name)
        Image{
            visible: sync_state
            anchors{
                right: parent.right
                bottom: parent.bottom
                bottomMargin: !Settings.nativeIcons ? 5 : 0
            }
            source: "../Images/synced.png"
            width: 25
            height: width
        }
    }


    Label{
        id: nameLabel
        color: Settings.inverted ? selected ? "white" : "black" : "white"
        anchors {
            left: thumb.right
            leftMargin: platformStyle.paddingMedium
            top: parent.top
            right: isDir ? indicator.left : parent.right
            margins: platformStyle.paddingMedium
        }
        verticalAlignment: Text.AlignTop
        elide: Text.ElideRight
        font.pixelSize: platformStyle.fontSizeLarge
        text: name
    }


    Label{
        id: sizeLabel
        platformInverted: Settings.inverted
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? platformStyle.paddingMedium : platformStyle.paddingLarge
            bottom: parent.bottom
            margins: platformStyle.paddingMedium
        }
        text: qsTr("size: %1").arg(Helper.convSize(size))
        font.pixelSize: platformStyle.fontSizeSmall
        color: selected ? "white" : platformStyle.colorNormalMid
    }


    MouseArea{
        id: mouseArea
        anchors {
            fill: parent
        }
        onPressed: {
            Helper.playEffect(Helper.ThemeBasicButton)
            list.currentIndex = index
            Session.currentIndex = index
        }
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress)
            root.pressAndHold()
        }
    }
}
