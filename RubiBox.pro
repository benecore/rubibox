TEMPLATE = app
QT += network sql
VERSION = 2.0.6
INCLUDEPATH += src
CONFIG += mobility
MOBILITY += feedback

#DEFINES += AUTH_IS_SET

SOURCES += main.cpp \
    src/boxapi.cpp \
    src/json/jsonparser.cpp \
    src/json/json.cpp \
    src/session.cpp \
    src/storage/database.cpp \
    src/storage/settings.cpp \
    src/factory.cpp \
    src/helper.cpp \
    src/qupfile.cpp \
    src/filebrowser.cpp \
    src/filemanager.cpp \
    src/filemanageritem.cpp \
    src/crypt/simplecrypt.cpp \
    src/model/model.cpp \
    src/model/items/transferitem.cpp \
    src/model/items/boxitem.cpp \
    src/model/sortmodel.cpp

HEADERS += \
    src/constants.h \
    src/boxapi.h \
    src/json/json.h \
    src/session.h \
    src/storage/database.h \
    src/storage/settings.h \
    src/constants.h \
    src/factory.h \
    src/helper.h \
    src/qupfile.h \
    src/filebrowser.h \
    src/filemanager.h \
    src/filemanageritem.h \
    src/crypt/simplecrypt.h \
    src/model/model.h \
    src/model/items/transferitem.h \
    src/model/items/boxitem.h \
    src/model/sortmodel.h


OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/preinst


simulator{
    folder_01.source = qml/RubiBox
    folder_01.target = qml
    DEPLOYMENTFOLDERS = folder_01

    DEFINES += APP_VERSION=\\\"$$VERSION\\\"
}


contains(MEEGO_EDITION,harmattan){
    message(MeeGo build)
    TARGET = RubiBox
    CONFIG += qdeclarative-boostable shareuiinterface-maemo-meegotouch mdatauri
    DEFINES += Q_OS_HARMATTAN
    DEFINES += APP_VERSION=\\\"$$VERSION\\\"
    SOURCES *= src/shareui.cpp
    HEADERS *= src/shareui.h
    #RESOURCES += harmattan.qrc
    splash.path = /opt/$${TARGET}/share/
    splash.files = splash/splash_port.png splash/splash_land.png
    #database.path = /home/user/$${TARGET}/
    #database.files = database/rubibox.db
    INSTALLS += splash
    #folder_01.source = qml/RubiBox
    #folder_01.target = qml
    #DEPLOYMENTFOLDERS = folder_01
    RESOURCES += harmattan.qrc
}


symbian:{
    message(Symbian build)
    CONFIG += qt-components
    TARGET = RubiBox_0x2006C519
    # Publish the app version to source code.
    DEFINES += APP_VERSION=\"$$VERSION\"
    ICON = RubiBox.svg
    TARGET.UID3 = 0x2006C519
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x1000 0x2000000 # 32MB
    TARGET.CAPABILITY += NetworkServices \
                         SwEvent \
                         ReadUserData \
                         WriteUserData \
                         LocalServices \
                         UserEnvironment
    DEPLOYMENT.display_name = RubiBox
    my_deployment.pkg_prerules += vendorinfo
    vendorinfo += "%{\"DevPDA\"}" ":\"DevPDA\""
    #folder_01.source = qml/RubiBox
    #folder_01.target = qml
    #DEPLOYMENTFOLDERS = folder_01
    DEPLOYMENT += my_deployment
    RESOURCES += symbian.qrc
}

include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()
