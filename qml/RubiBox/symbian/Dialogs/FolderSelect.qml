// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1
import com.devpda.models 1.0

import "../../common"
import "../Delegates"
import "../Component"


Sheet{
    id: root


    FileManager{
        id: fileMan
        onIsRootChanged: console.debug("is root: "+isRoot)
    }



    rejectButtonText: qsTr("Cancel")
    acceptButtonText: qsTr("Select")

    onAccepted: {
        if (root.destroyWhenClosed){
            root.destroy(600);
        }
        Settings.downloadFolder = fileMan.folder
    }

    onRejected: if (root.destroyWhenClosed) root.destroy(600);


    content: Item {
        anchors.fill: parent

        Rectangle{
            id: header
            width: parent.width
            height: 50
            color: Settings.activeColor
            clip: true
            z: 1000

            Label{
                platformInverted: Settings.inverted
                anchors{
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    margins: platformStyle.paddingLarge
                }
                text: fileMan.folder
                elide: Text.ElideLeft
                color: "white"
                font.pixelSize: inPortrait ?  platformStyle.fontSizeLarge : platformStyle.paddingSmall
            }
        }


        ListView{
            id: list
            anchors.fill: parent
            anchors.topMargin: header.height
            anchors.bottomMargin: bottomHeader.height

            model: fileMan
            delegate: FolderSelectDelegate{
                onClicked: {
                    fileMan.showOnlyDirs = true
                    fileMan.folder = path
                }
            }
        }

        Rectangle{
            id: bottomHeader
            anchors{
                bottom: parent.bottom
            }
            width: parent.width
            height: backBtn.height
            color: Settings.activeColor
            clip: true
            z: 1000
            Button{
                id: backBtn
                platformInverted: Settings.inverted
                enabled: (!fileMan.isRoot)
                width: inPortrait ? parent.width/2 : parent.height/2
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Back")
                onClicked: {
                    Helper.playEffect(Helper.ThemeBasicButton)
                    fileMan.back()
                }
            }
        }
    }
}
