#include "filemanageritem.h"

FileManagerItem::FileManagerItem(QFileInfo &info, QObject *parent) :
    QObject(parent), m_info(info), m_selected(false)
{
    emit dataChanged();
}
