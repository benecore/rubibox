// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "Component"
import "../scripts/createobject.js" as ObjectCreator
import "../scripts/const.js" as Const
import "../scripts/utils.js" as Utils
import "FastScroll"
import "Delegates"
import "Buttons"
import "ToolBars"
import "Settings"

DevPDA{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
    }



    devText: qsTr("Settings")
    loadingVisible: false


    ListModel{
        id: listModel
        ListElement {name: ""; fileName: "Settings/GeneralSettings.qml"}
        ListElement {name: ""; fileName: "Settings/AccountsSettings.qml"}
        ListElement {name: ""; fileName: "Settings/TransferSettings.qml"}
        ListElement {name: ""; fileName: "Settings/AppearanceSettings.qml"}
        ListElement {name: ""; fileName: "Settings/AboutPage.qml"}

        Component.onCompleted: {
            listModel.get(0).name = qsTr("General")
            listModel.get(1).name = qsTr("Accounts")
            listModel.get(2).name = qsTr("Transfers")
            listModel.get(3).name = qsTr("Appearance")
            listModel.get(4).name = qsTr("About")
        }
    }


    ListView{
        id: list
        anchors {
            fill: parent
            topMargin: devMargin
        }
        model: listModel
        delegate: BoxSettingsPageDelegate{
            onClicked: root.pageStack.push(Qt.resolvedUrl(fileName))
        }
    }

}
