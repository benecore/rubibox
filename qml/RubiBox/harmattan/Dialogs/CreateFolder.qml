// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../../common"
import "../../scripts/const.js" as Const

Sheet{
    id: root



    rejectButtonText: qsTr("Cancel")
    acceptButtonText: qsTr("Create")
    platformStyle: SheetStyle{
        background: appStyle.background
    }

    onAccepted: {
        if (root.destroyWhenClosed){
            root.destroy(600);
        }
        if (field.text !== ""){
            Session.createFolder(field.text)
        }else{
            infoBanner.parent = root
            infoBanner.text = qsTr("Folder name can't be empty")
            infoBanner.show()
        }
    }

    onRejected: if (root.destroyWhenClosed) root.destroy(600);


    onStatusChanged: {
        if (status == DialogStatus.Opening){
            field.text = ""
            field.focus = true
        }
    }

    content: Item{
        anchors.fill: parent
        anchors.margins: Const.PADDING_LARGE

        Column{
            width: parent.width
            spacing: 10

            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Const.FONT_LARGE
                text: qsTr("Create folder")
                color: !Settings.inverted ? "black" : "white"
            }

            HLine{
                width: parent.width
            }

            TextField{
                id: field
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width-(Const.PADDING_LARGE*2)
                placeholderText: qsTr("folder name")
                inputMethodHints: Qt.ImhNoPredictiveText
                echoMode: TextInput.Normal
                validator: RegExpValidator{regExp: /[^\\//|:?*<>]*/}
                Keys.onReturnPressed: {
                    if (text !== ""){
                        focus = false;
                        platformCloseSoftwareInputPanel()
                        root.accept()
                    }else{
                        infoBanner.parent = root
                        infoBanner.text = qsTr("Folder name can't be empty")
                        infoBanner.show()
                    }
                }
                onFocusChanged: focus ? platformOpenSoftwareInputPanel() : platformCloseSoftwareInputPanel()
            }
        }
    }
}
