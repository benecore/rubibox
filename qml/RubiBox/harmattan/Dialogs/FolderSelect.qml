// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.devpda.models 1.0

import "../../common"
import "../Delegates"
import "../../scripts/const.js" as Const

Sheet{
    id: root


    FileManager{
        id: fileMan
        onIsRootChanged: console.debug("is root: "+isRoot)
    }



    rejectButtonText: qsTr("Cancel")
    acceptButtonText: qsTr("Select")
    platformStyle: SheetStyle{
        background: appStyle.background
    }

    onAccepted: {
        if (root.destroyWhenClosed){
            root.destroy(600);
        }
        Settings.downloadFolder = fileMan.folder
    }

    onRejected: if (root.destroyWhenClosed) root.destroy(600);


    content: Item {
        anchors.fill: parent

        Rectangle{
            id: header
            width: parent.width
            height: inPortrait ? Const.HEADER_DEFAULT_HEIGHT_PORTRAIT : Const.HEADER_DEFAULT_HEIGHT_LANDSCAPE
            color: Settings.activeColor
            clip: true
            z: 1000

            Label{
                anchors{
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    margins: Const.PADDING_LARGE
                }
                text: fileMan.folder
                elide: Text.ElideLeft
                color: "white"
                font.pixelSize: inPortrait ?  Const.FONT_LARGE : Const.FONT_SMALL
            }
        }


        ListView{
            id: list
            anchors.fill: parent
            anchors.topMargin: header.height
            anchors.bottomMargin: bottomHeader.height

            model: fileMan
            delegate: FolderSelectDelegate{
                onClicked: {
                    fileMan.showOnlyDirs = true
                    fileMan.folder = path
                }
            }
        }

        Rectangle{
            id: bottomHeader
            anchors{
                bottom: parent.bottom
            }
            width: parent.width
            height: backBtn.height
            color: Settings.activeColor
            clip: true
            z: 1000
            Button{
                id: backBtn
                enabled: (fileMan.folder !== "/home/user")
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Back")
                onClicked: {
                    Helper.playEffect(Helper.ThemeBasicButton)
                    fileMan.back()
                }
            }
        }
    }
}
