#include "filebrowser.h"


class FBrowserPrivate
{
public:
    FBrowserPrivate() :
        sortField(FBrowser::Name), sortReversed(false), count(0){
        nameFilters << QLatin1String("*");
    }

    void updateSorting() {
        QDir::SortFlags flags = 0;
        switch(sortField) {
        case FBrowser::Unsorted:
            flags |= QDir::Unsorted;
            break;
        case FBrowser::Name:
            flags |= QDir::Name;
            break;
        case FBrowser::Time:
            flags |= QDir::Time;
            break;
        case FBrowser::Size:
            flags |= QDir::Size;
            break;
        case FBrowser::Type:
            flags |= QDir::Type;
            break;
        }
        if (sortReversed)
            flags |= QDir::Reversed;

        model.setSorting(flags);
    }



    QDirModel model;
    QUrl folder;
    QStringList nameFilters;
    QModelIndex folderIndex;
    FBrowser::SortField sortField;
    bool sortReversed;
    int count;
};


FBrowser::FBrowser(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray>roles;
    roles[NAME] = "name";
    roles[PATH] = "path";
    roles[SIZE] = "size";
    roles[SUFFIX] = "suffix";
    roles[IS_DIR] = "isDir";
    roles[IS_HIDDEN] = "isHidden";
    setRoleNames(roles);

    d = new FBrowserPrivate;
    d->model.setFilter(QDir::AllDirs | QDir::Files | QDir::Drives | QDir::NoDotAndDotDot);
    connect(&d->model, SIGNAL(rowsInserted(const QModelIndex&,int,int))
            , this, SLOT(inserted(const QModelIndex&,int,int)));
    connect(&d->model, SIGNAL(rowsRemoved(const QModelIndex&,int,int))
            , this, SLOT(removed(const QModelIndex&,int,int)));
    connect(&d->model, SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&))
            , this, SLOT(handleDataChanged(const QModelIndex&,const QModelIndex&)));
    connect(&d->model, SIGNAL(modelReset()), this, SLOT(refresh()));
    connect(&d->model, SIGNAL(layoutChanged()), this, SLOT(refresh()));
    /*
#ifdef HARMATTAN_BOOSTER
    listFolder("/home/user");
#else
    listFolder();
#endif
*/
}

FBrowser::~FBrowser()
{
    delete d;
}

QVariant FBrowser::data(const QModelIndex &index, int role) const
{
    QFileInfo info;
    QModelIndex modelIndex = d->model.index(index.row(), 0, d->folderIndex);
    if(!modelIndex.isValid())
        return QVariant();
    switch (role){
    case NAME:
        return d->model.data(modelIndex, QDirModel::FileNameRole).toString();
    case PATH:
        return QUrl::fromLocalFile(d->model.data(modelIndex, QDirModel::FilePathRole).toString());
    case SIZE:
        info.setFile(d->model.data(modelIndex, QDirModel::FilePathRole).toString());
        return info.size();
    case SUFFIX:
        info.setFile(d->model.data(modelIndex, QDirModel::FilePathRole).toString());
        return info.completeSuffix();
    case IS_DIR:
        return d->model.isDir(modelIndex);
    case IS_HIDDEN:
        info.setFile(d->model.data(modelIndex, QDirModel::FilePathRole).toString());
        return info.isHidden();
    default:
        return QVariant();
    }
}

int FBrowser::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return d->count;
}

/*
void FBrowser::listFolder(const QString path)
{
    clear();
    reset();
    m_folder = QDir(path).absolutePath();
    qDebug() << path;
    QFileInfoList list = QDir(m_folder).entryInfoList(QDir::AllEntries | QDir::Writable | QDir::Readable | QDir::NoDot | QDir::NoDotDot,QDir::DirsFirst| QDir::Name);
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    foreach(QFileInfo temp, list){
        m_list << new QFileInfo(temp);
    }
    endInsertRows();
}



void FBrowser::back()
{
    QDir qDir = QDir(m_folder);
    qDir.cdUp();
    listFolder(qDir.path());
}
*/

void FBrowser::classBegin()
{
}

void FBrowser::componentComplete()
{
    if (!d->folder.isValid() || d->folder.toLocalFile().isEmpty() || !QDir().exists(d->folder.toLocalFile()))
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_WIN)
        setFolder(QUrl(QLatin1String("file://")+QDir::currentPath()));
#elif defined(Q_OS_HARMATTAN)
        setFolder(QUrl(QLatin1String("file://")+QDir::homePath()));
#endif

    if (!d->folderIndex.isValid())
        QMetaObject::invokeMethod(this, "refresh", Qt::QueuedConnection);
}


QUrl FBrowser::folder() const
{
    return d->folder;
}


void FBrowser::setFolder(const QUrl &folder)
{
    if (folder == d->folder)
        return;
    QModelIndex index = d->model.index(folder.toLocalFile());
    if ((index.isValid() && d->model.isDir(index)) || folder.toLocalFile().isEmpty()) {

        d->folder = folder;
        QMetaObject::invokeMethod(this, "refresh", Qt::QueuedConnection);
        emit folderChanged();
    }
}


QUrl FBrowser::parentFolder() const
{
    QString localFile = d->folder.toLocalFile();
    if (!localFile.isEmpty()) {
        QDir dir(localFile);
#if defined(Q_OS_SYMBIAN) || defined(Q_OS_WIN)
        if (dir.isRoot())
            dir.setPath("");
        else
#endif
            dir.cdUp();
        localFile = dir.path();
    } else {
        int pos = d->folder.path().lastIndexOf(QLatin1Char('/'));
        if (pos == -1)
            return QUrl();
        localFile = d->folder.path().left(pos);
    }
    return QUrl::fromLocalFile(localFile);
}


QStringList FBrowser::nameFilters() const
{
    return d->nameFilters;
}

void FBrowser::setNameFilters(const QStringList &filters)
{
    d->nameFilters = filters;
    d->model.setNameFilters(d->nameFilters);
}

FBrowser::SortField FBrowser::sortField() const
{
    return d->sortField;
}

void FBrowser::setSortField(FBrowser::SortField field)
{
    if (field != d->sortField) {
        d->sortField = field;
        d->updateSorting();
    }
}

bool FBrowser::sortReversed() const
{
    return d->sortReversed;
}

void FBrowser::setSortReversed(bool rev)
{
    if (rev != d->sortReversed) {
        d->sortReversed = rev;
        d->updateSorting();
    }
}

bool FBrowser::showDirs() const
{
    return d->model.filter() & QDir::AllDirs;
}


void FBrowser::setShowDirs(bool on)
{
    if (!(d->model.filter() & QDir::AllDirs) == !on)
        return;
    if (on)
        d->model.setFilter(d->model.filter() | QDir::AllDirs | QDir::Drives);
    else
        d->model.setFilter(d->model.filter() & ~(QDir::AllDirs | QDir::Drives));
}


bool FBrowser::showHidden() const
{
    return d->model.filter() & QDir::Hidden;
}


void FBrowser::setShowHidden(bool on)
{
    if (!(d->model.filter() & QDir::Hidden) == !on){
        return;
    }
    if (on){
        d->model.setFilter(d->model.filter() | QDir::Hidden);
    }else{
        d->model.setFilter(d->model.filter() & ~QDir::Hidden);
    }
}


bool FBrowser::showOnlyDirs() const
{
    return d->model.filter() & QDir::Dirs;
}


void FBrowser::setShowOnlyDirs(bool on)
{
    if (!(d->model.filter() & QDir::Dirs) == !on){
        return;
    }
    if (on){
        d->model.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    }else{
        d->model.setFilter(d->model.filter() & ~QDir::Dirs);

    }
}


void FBrowser::handleDataChanged(const QModelIndex &start, const QModelIndex &end)
{
    if (start.parent() == d->folderIndex){
        emit dataChanged(index(start.row(),0), index(end.row(),0));
    }
}

void FBrowser::inserted(const QModelIndex &index, int start, int end)
{
    if (index == d->folderIndex) {
        emit beginRemoveRows(QModelIndex(), start, end);
        d->count = d->model.rowCount(d->folderIndex);
        emit endRemoveRows();
        emit countChanged();
    }
}


void FBrowser::removed(const QModelIndex &index, int start, int end)
{
    if (index == d->folderIndex) {
        emit beginRemoveRows(QModelIndex(), start, end);
        d->count = d->model.rowCount(d->folderIndex);
        emit endRemoveRows();
        emit countChanged();
    }
}


void FBrowser::refresh()
{
    d->folderIndex = QModelIndex();
    if (d->count) {
        emit beginRemoveRows(QModelIndex(), 0, d->count-1);
        d->count = 0;
        emit endRemoveRows();
    }
    d->folderIndex = d->model.index(d->folder.toLocalFile());
    int newcount = d->model.rowCount(d->folderIndex);
    if (newcount) {
        emit beginInsertRows(QModelIndex(), 0, newcount-1);
        d->count = newcount;
        emit endInsertRows();

    }
}
