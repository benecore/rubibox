#ifndef TRANSFERITEM_H
#define TRANSFERITEM_H

#include "model/model.h"
#include <QObject>
#include <QVariantMap>
#include <QStringList>
#include <QFileInfo>


class TransferItem : public ModelItem
{
public:
    TransferItem(QObject *parent = 0) : ModelItem(parent) {}
    explicit TransferItem(const QVariant &id, const QVariant &name, const QVariant &size, QObject *parent = 0);
    explicit TransferItem(const QVariant &path, const QVariant &size, QObject *parent = 0);


    enum Roles{
        NAME,
        ID,
        RECEIVED_SIZE,
        SIZE,
        ACTIVE,
        ERROR,
        ERROR_STRING,
        DOWNLOADING,
        SPEED,
        TO_FOLDER,
        PATH
    };

    QVariant data(int role) const;
    QHash<int, QByteArray> roleNames() const;
    void setProperty(const QVariant &role, const QVariant &value) { Q_UNUSED(role) Q_UNUSED(value)}


public slots:
    inline QVariant identi() const {
        if (downloading()){
            return id();
        }else{
            return name();
        }
    }
    inline QVariant name() const { return _name; }
    inline QVariant id() const { return _id; }
    inline qint64 received_size() const { return _received_size; }
    inline QVariant size() const { return _size; }
    inline QVariant speed() const { return _speed; }
    inline bool active() const { return _active; }
    inline bool error() const { return _error; }
    inline bool downloading() const { return _downloading; }
    inline QVariant toFolder() const { return _toFolder; }
    inline QVariant path() const { return _path; }

    void setReceivedSize(const qint64 &received_size);
    void setActive(const bool &active);
    void setDownloading(const bool &downloading);
    void setToFolder(const QString &toFolder);
    void setSpeed(const QString &speed);


private:
    QVariant _name,
    _id,
    _size,
    _speed,
    _toFolder,
    _path;
    qint64 _received_size;
    bool _active,
    _error,
    _downloading; // true if is Download

};

#endif // TRANSFERITEM_H
