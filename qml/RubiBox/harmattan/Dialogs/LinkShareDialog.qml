// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/createobject.js" as ObjectCreator
import "../../scripts/const.js" as Const

Dialog{
    id: root


    property string shareLink


    content: Item {
        id: deleteName
        height: 50
        width: parent.width
        Label {
            id: deleteText
            width: parent.width
            elide: Text.ElideMiddle
            font.pixelSize: Const.FONT_LARGE
            font.bold: true
            color: Settings.activeColor
            text: shareLink
        }
    }

    buttons:  ButtonColumn {
        id: buttonsDelete
        spacing: 5
        style: ButtonStyle { }
        anchors.horizontalCenter: parent.horizontalCenter
        exclusive: false
        Button {
            text: qsTr("Copy")
            onClicked: {
                Helper.playEffect()
                Helper.copy(shareLink)
                root.accept()
            }
        }
        Button {
            text: qsTr("Share")
            onClicked: {
                Helper.playEffect()
                shareUI.share(getCurrentItem().name, shareLink)
                root.accept()
            }
        }
        Button {text: qsTr("Cancel"); onClicked: root.close()}
    }

}
