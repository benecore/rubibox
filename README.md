## RubiBox fully-featured Box.com client for Symbian/MeeGo ##

**RubiBox** native application for the Nokia smartphones with Symbian and MeeGo/`(Harmattan)` OS

**RubiBox** is winning application of *Openmobility Summer Contest to Win a Linux Smartphone (2012)*
[http://www.openmobility.cz/openmobility/the-winners-are-known/](http://www.openmobility.cz/openmobility/the-winners-are-known/ "Summer Contest to Win a Linux Smartphone (2012)")

![Screenshot](https://bitbucket.org/benecore/rubibox/raw/eb61ebaf58f0ca9a430ffda6da209d5ab393561e/screen.png)

Release notes:
-
### v2.0.6 (`29.12.2013`)
- Critical bug fixed when uploading files to empty folder
### v2.0.5 (`28.12.2013`)
- Ability to change SYNC state for folders
- Ability to change name when COPY/MOVE items exists
- Sorted model (CaseSensitive) - prepared for next update
- Fixed login issue
- Minor fixes
### v2.0.3 (`30.10.2013`)
- Fixed saving items limit
- Minor fixes
### v2.0.2 (`26.10.2013`)
- New style of page header
- Ability to change the number of folder items - General settings
- Ability to Load more (btn) items
- Fixed the number of folder items (issue #1)
- Fixed download folder change - Symbian
- Minor fixes
### v2.0.1 (`01.06.2013`)
- Fixed changes of Box.com API for list folders
- Minor fixes
### v2.0.0 (`06.05.2013`)
- Application was completely rewritten
- Copy/Move actions
- Support multiple downloads/uploads
- Support for multiple accounts
- UI improvements with color themes etc...
- and more
### v1.0.1 (`10.10.2012`)
- File browser improvements
- Minimized JavaScrip code for quick responses
- Privacy Policy `(Nokia Store QA)`
- First start page with little help ;)
- Minor fixes
### v1.0.0 (`30.09.2012`)
- Initial version