import QtQuick 1.1
import com.nokia.symbian 1.1


Item {
    id: root

    property alias text: label.text

    height: 20

    Label {
        id: label
        anchors { right: parent.right; top: parent.top }
        font.pixelSize: platformStyle.fontSizeSmall
        font.bold: true
        color: platformStyle.colorNormalMid
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        height: 1
        anchors { left: parent.left; right: label.left; rightMargin: 10 ; verticalCenter: label.verticalCenter }
        color: platformStyle.colorNormalMid
    }
}
