// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../common"


Item{
    id: root
    width: ListView.view.width

    height: active ? nameLabel.height + sizeLabel.height + (platformStyle.paddingMedium*5)
                   : nameLabel.height + sizeLabel.height + (platformStyle.paddingMedium*3)

    Behavior on height {PropertyAnimation{duration: 500}}


    signal clicked
    signal pressAndHold


    HLine{
        anchors.bottom: parent.bottom
    }


    Image{
        id: thumb
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: platformStyle.paddingSmall
        }
        sourceSize.width: !Settings.nativeIcons ? platformStyle.graphicSizeLarge : 55
        sourceSize.height: !Settings.nativeIcons ? platformStyle.graphicSizeLarge : 55
        width: sourceSize.width
        height: sourceSize.height
        smooth: true
        source: Helper.getIcon(!Settings.inverted, false, 2, Settings.nativeIcons, name)
    }

    Label{
        id: nameLabel
        platformInverted: Settings.inverted
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? platformStyle.paddingSmall : platformStyle.paddingMedium
            top: parent.top
            right: parent.right
            margins: platformStyle.paddingSmall
        }
        verticalAlignment: Text.AlignTop
        elide: Text.ElideRight
        font.pixelSize: platformStyle.fontSizeLarge
        text: name
    }


    Image{
        id: transferIcon
        anchors{
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? platformStyle.paddingSmall : platformStyle.paddingMedium
            top: nameLabel.bottom
            margins: platformStyle.paddingSmall
        }
        sourceSize.width: platformStyle.graphicSizeSmall
        sourceSize.height: platformStyle.graphicSizeSmall
        smooth: true
        source: download ? "../Images/download.png" : "../Images/upload.png"
    }


    Label{
        id: sizeLabel
        anchors {
            left: transferIcon.right
            leftMargin: platformStyle.paddingMedium
            right: parent.right
            top: nameLabel.bottom
            margins: platformStyle.paddingSmall
        }
        text: qsTr("%1 / %2 | %3").arg(Helper.convSize(received_size)).arg(Helper.convSize(size)).arg(speed)
        font.pixelSize: platformStyle.fontSizeSmall-2
        color: platformStyle.colorNormalMid
    }

    ProgressBar{
        id: progressBar
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? platformStyle.paddingSmall : platformStyle.paddingMedium
            right: parent.right
            top: sizeLabel.bottom
            margins: platformStyle.paddingMedium
        }
        minimumValue: 0
        maximumValue: 100
        value: (received_size*100)/size
        opacity: active ? 1 : 0

        Behavior on opacity {PropertyAnimation{duration: 500}}
    }



    MouseArea{
        id: mouseArea
        anchors {
            fill: parent
        }
        onPressed: {
            Helper.playEffect(Helper.ThemeBasicButton)
            list.currentIndex = index
        }
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress)
            root.pressAndHold()
        }
    }

}
