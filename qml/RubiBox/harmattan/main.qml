// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "FirstStart"
import "../scripts/utils.js" as Utils
import "../scripts/createobject.js" as ObjectCreator

PageStackWindow{
    id: window

    function checkConflict(name){
        Utils.CM_ITEM_CONFLICT = false;
        for (var i = 0; i < BModel.count; i++){
            if (name == BModel.get(i).name){
                Utils.CM_ITEM_CONFLICT = true
                console.log("ITEM CONFLICT: ".concat(Utils.CM_ITEM_CONFLICT))
                return;
            }
        }
        console.log("ITEM CONFLICT: ".concat(Utils.CM_ITEM_CONFLICT))
    }


    function showConflictdialog(){
        Helper.playEffect(Helper.ThemePopupOpen)
        function okClicked(name){
            if (name === ""){
                infoBanner.parent = window
                infoBanner.text = qsTr("New name can't be empty")
                infoBanner.show()
                return;
            }
            checkConflict(name)
            if (Utils.CM_ITEM_CONFLICT){
                infoBanner.parent = window
                infoBanner.text = qsTr("To continue you need to change file name")
                infoBanner.show()
                return;
            }
            if (Session.action === "copy"){
                Session.copyItem(name)
            }
            else if (Session.action === "move"){
                Session.moveItem(name)
            }
        }
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/SimpleInput.qml"), pageStack)
        dialog.okClicked.connect(okClicked)
        dialog.placeHolder = Utils.CM_ITEM_NAME
        dialog.open()
    }

    function getCurrentItem(){
        return BModel.get(Session.currentIndex);
    }


    function _selectAllFiles(){
        for (var i = 0; i < BModel.count; ++i){
            BModel.setProperty(i, "selected", true)
        }
    }


    function _deselectAllFiles(){
        for (var i = 0; i < BModel.count; ++i){
            BModel.setProperty(i, "selected", false)
        }
    }


    function addToTransferQueue(id, name, size){
        Utils.addToQueue(id, name, size)
    }


    function showLinkShareDialog(shareLink){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("Dialogs/LinkShareDialog.qml"), window.pageStack)
        dialog.shareLink = shareLink
        dialog.open()
    }


    showStatusBar: inPortrait
    showToolBar: true
    initialPage: Database.authorized ? boxPage : Qt.resolvedUrl("FirstStart/BoxFirstPage.qml")
    platformStyle: PageStackWindowStyle{
        id: appStyle
        cornersVisible: true
        background: Settings.inverted ? 'image://theme/meegotouch-video-background' : 'image://theme/meegotouch-applicationpage-background' + __invertedString
        backgroundFillMode: Image.Stretch
    }

    property bool isItemSelected: false


    BoxPage{
        id: boxPage
    }


    InfoBanner{
        id: infoBanner
        z: 1000
        iconSource: "Images/bannerImage.png"
    }


    Connections{
        target: Settings
        onInvertedChanged: theme.inverted = value
        onMultiSelectionChanged: {
            infoBanner.parent = pageStack
            if (value){
                infoBanner.text = qsTr("Multiselection enabled");
            }else{
                _deselectAllFiles()
                infoBanner.text = qsTr("Multiselection disabled");
            }
            infoBanner.show()
        }
        onItemsLimitChanged: {
            infoBanner.parent = pageStack
            infoBanner.text = qsTr("Items limit saved");
            infoBanner.show()
        }
    }


    Connections{
        target: Box
        onCopyActionDone: Session.action = "normal"
        onMoveActionDone: {
            Session.action = "normal"
        }

        onAllTransfersDone: {
            infoBanner.parent = pageStack
            infoBanner.text = qsTr("All transfers finished");
            infoBanner.show()
        }
        onShareLinkDone: {
            getCurrentItem().share_link = shareLink
            showLinkShareDialog(shareLink)
        }
    }


    Connections{
        target: Database
        onActiveAccountChanged: Session.listFolder()
    }



    Component.onCompleted: {
        if (Database.authorized)
            Session.renewToken()
        Helper.playEffect()
        theme.inverted = Settings.inverted
        theme.colorScheme = Settings.activeColorScheme
        console.debug(Database.usePassword+"\n"+Database.password)
    }

}
