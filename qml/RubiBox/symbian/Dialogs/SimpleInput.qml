// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

import "../../scripts/utils.js" as Utils

CommonDialog{
    id: root

    property alias placeHolder: field.placeholderText
    property alias text: field.text
    property alias titleText: root.titleText

    signal okClicked(string name);

    platformInverted: window.platformInverted
    titleIcon: platformInverted ? "../Images/accept_black.png" :"../Images/accept_white.png"
    buttonTexts: [qsTr("Continue"), qsTr("Cancel")]
    height: 190
    onButtonClicked: {
        if (index == 0){
            window.checkConflict(field.text)
            if (Utils.CM_ITEM_CONFLICT){
                infoBanner.text = qsTr("To continue you need to change file name")
                infoBanner.open()
                return;
            }else{
                root.okClicked(field.text)
            }
        }else{
            Session.action = "normal"
        }
    }


    content: Item{
        anchors.left: parent.left
        anchors.right: parent.right

        TextField{
            id: field
            focus: true
            anchors {top: parent.bottom; topMargin: platformStyle.paddingLarge; horizontalCenter: parent.horizontalCenter;}
            width: parent.width-(platformStyle.paddingMedium*2)
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
        }
    }
}
