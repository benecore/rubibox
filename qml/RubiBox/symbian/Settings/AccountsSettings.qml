import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../Component"
import "../Delegates"
import "../Buttons"
import "../../scripts/createobject.js" as ObjectCreator

DevPDA{
    id: root


    function addAccount(username){
        if (Database.addAccount(username, 0)){
            var addAccount = ObjectCreator.createObject(Qt.resolvedUrl("../Auth/AddAccount.qml"), root.pageStack)
            addAccount.accountName = username
            addAccount.open()
            usernameInput.text = "";
            usernameInput.closeSoftwareInputPanel()
            flick.focus = true;
            return;
        }
        console.debug("UNABLE ADD ACCOUNT")
    }


    function setActiveAccount(){
        Database.setAsActive(accountsModel.get(listView.currentIndex).name)
    }


    function showRemoveDialog(itemName){
        var dialog = ObjectCreator.createObject(Qt.resolvedUrl("../Dialogs/AccountRemoveDialog.qml"), root.pageStack)
        dialog.itemName = itemName
        dialog.open()
    }


    tools: ToolBarLayout{
        ToolIcon{
            iconSource: "toolbar-previous"
            onClicked: root.pageStack.pop()
        }
        ToolIcon{
            iconSource: "toolbar-menu"
            onClicked: if (menu.status === DialogStatus.Open) {menu.close()} else {menu.open()}
        }
    }



    ContextMenu{
        id: contextMenu

        property string itemName

        platformInverted: Settings.inverted
        MenuLayout{
            MenuItem{
                text: qsTr("Set as active")
                platformInverted: Settings.inverted
                onClicked: {
                    Helper.playEffect()
                    Database.setAsActive(contextMenu.itemName)
                }
            }
            MenuItem{
                text: qsTr("Delete")
                platformInverted: Settings.inverted
                onClicked: {
                    Helper.playEffect()
                    showRemoveDialog(contextMenu.itemName)
                }
            }
        }
    }



    Menu{
        id: menu
        platformInverted: Settings.inverted
        MenuLayout{
            MenuItem{
                platformInverted: Settings.inverted
                text: qsTr("Delete all accounts");
                onClicked: {
                    if (Database.clearAllAccounts()){
                        root.pageStack.replace(Qt.resolvedUrl("../FirstStart/BoxFirstPage.qml"))
                    }else{
                        infoBanner.parent = root.pageStack
                        infoBanner.text = qsTr("Unable delete accounts")
                        infoBanner.open()
                    }
                }
            }
        }
    }


    devText: qsTr("Settings - Accounts")
    loadingVisible: false
    onStatusChanged: {
        if (status === PageStatus.Activating && !accountsModel.count){
            var list = Database.accounts
            for (var index in list){
                accountsModel.append({"name": list[index]})
            }
        }
    }


    Connections{
        target: Database
        onAccountsCountChanged: {
            accountsModel.clear()
            var list = Database.accounts
            for (var index in list){
                accountsModel.append({"name": list[index]})
            }
        }
        onActiveAccountChanged: {
            accountsModel.clear()
            var list = Database.accounts
            for (var index in list){
                accountsModel.append({"name": list[index]})
            }
        }
    }


    Flickable{
        id: flick
        anchors {
            fill: parent
            topMargin: devMargin
            margins: platformStyle.paddingMedium
        }
        contentHeight: content.height
        flickableDirection: Flickable.VerticalFlick


        Column{
            id: content
            anchors{
                left: parent.left
                right: parent.right
            }

            spacing: 10

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Add account")
            }

            Row{
                spacing: 5
                width: parent.width

                TextField{
                    id: usernameInput
                    platformInverted: Settings.inverted
                    width: parent.width - addBtn.width
                    placeholderText: qsTr("Display name")
                    inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                    Keys.onEnterPressed: {
                        closeSoftwareInputPanel()
                        addAccount(text)
                    }
                    Keys.onReturnPressed: {
                        closeSoftwareInputPanel()
                        addAccount(text)
                    }
                }
                Button{
                    id: addBtn
                    platformInverted: Settings.inverted
                    width: height
                    iconSource: Settings.inverted ? "../Images/add.png" : "../Images/add_white.png"
                    onClicked: addAccount(usernameInput.text)
                    enabled: usernameInput.text != ""
                }
            }


            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Active account")
            }

            Row{
                anchors{
                    left: parent.left
                    right: parent.right
                }
                Label{
                    platformInverted: Settings.inverted
                    anchors.verticalCenter: infoBtn.verticalCenter
                    font.bold: true
                    text: Database.active_account
                    width: parent.width - infoBtn.width
                    font.pixelSize: platformStyle.fontSizeLarge
                }
                Image{
                    id: infoBtn
                    source: Settings.inverted ? "../Images/info-inverted.png"
                                            : "../Images/info.png"
                    sourceSize.width: platformStyle.graphicSizeSmall
                    sourceSize.height: platformStyle.graphicSizeSmall
                    cache: false

                    MouseArea{
                        anchors.fill: parent
                        onPressed: Helper.playEffect(Helper.ThemeBasicButton)
                        onClicked: {
                            root.pageStack.push(Qt.resolvedUrl("../BoxAccountInfoPage.qml"))
                        }
                    }
                }
            }

            Separator{
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: qsTr("Others accounts")
            }


            ListView{
                id: listView
                visible: (Database.accountsCount > 1)
                width: parent.width
                height: 40 * (Database.accountsCount-1)
                model: ListModel{
                    id: accountsModel
                }
                delegate: AccountsSettingsDelegate{
                    id: accountsDelegate
                    onClicked: {
                        contextMenu.itemName = name
                        contextMenu.open()
                    }
                }
                interactive: false
                //clip: true
            }

            Label{
                id: noAccountsLabel
                platformInverted: Settings.inverted
                visible: !(Database.accountsCount > 1)
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("No others accounts")
                color: platformStyle.colorNormalMid
            }
        }
    }




}
