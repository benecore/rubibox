#include "helper.h"
#include <QDebug>

Helper::Helper(QObject *parent) :
    QObject(parent)
{
    clipBoard = QApplication::clipboard();
    crypt = new SimpleCrypt(MAGIC_NUMBER);
}







Helper::~Helper()
{
    delete crypt;
}



QString Helper::cryptText(QString &text)
{
    QString result = crypt->encryptToString(text);
    qDebug() << "Encrypted password: " << result;
    return result;
}


bool Helper::decryptText(QString &crypted, QString &check)
{
    QString decrypted = crypt->decryptToString(crypted);
    qDebug() << "Crypted: " << decrypted << "CHECK: " << check;
    return (decrypted == check);
}


QString Helper::convSize(const qint64 size)
{
    QString temp;
    if (size < 1025){
        temp.append(QString("%1 B").arg(size));
    }
    else if (1024 < size && size < 1048577){
        temp.append(QString("%1 kB").arg(QString::number(size/1024.0, 'f', 1)));
    }
    else if (1048576 < size && size < 1073741825){
        temp.append(QString("%1 MB").arg(QString::number(size/1048576.0, 'f', 2)));
    }
    else{
        temp.append(QString("%1 GB").arg(QString::number(size/1073741824.0, 'f', 2)));
    }
    return temp;
}


void Helper::playEffect(const QFeedbackEffect::ThemeEffect &type)
{
    QFeedbackEffect::playThemeEffect(type);
}


QString Helper::getIcon(const bool inverted,
                        const bool isFolder,
                        const int prepend,
                        const bool native,
                        const QString name)
{
#ifdef Q_OS_HARMATTAN
    QString path("harmattan/Images/");
#elif defined(Q_OS_SYMBIAN)
    QString path("symbian/Images/");
#else
    QString path("harmattan/Images/");
#endif
    switch (prepend){
    case 1:
        path.prepend("../");
        break;
    case 2:
        path.prepend("../../");
        break;
    default:
        break;
    }
    if (native){
        if (inverted){
            if (isFolder){
                path.append("native/folder.png");
            }else{
                path.append(QString("native/%1.png").arg(iconFromSuffix(QFileInfo(name).suffix())));
            }
        }else{
            if (isFolder){
                path.append("native/folder-inverted.png");
            }else{
                path.append(QString("native/%1-inverted.png").arg(iconFromSuffix(QFileInfo(name).suffix())));
            }
        }
    }
    else{
        if (inverted){
            if (isFolder){
                path.append("custom/folder-inverted.png");
            }else{
                path.append("custom/file-inverted.png");
            }
        }else{
            if (isFolder){
                path.append("custom/folder.png");
            }else{
                path.append("custom/file.png");
            }
        }
    }
    return path;
}


qint64 Helper::getSize(QUrl path)
{
    /*
    //QFileInfo info(path.toLocalFile());
    QDir dir(path);
    qint64 size;
    QFileInfoList list = dir.entryInfoList();
    for (int iList = 0; iList < list.count() ; iList++)
    {
        QFileInfo info = list[iList];
        QString sFilePath = info.filePath();
        if (info.isDir())
        {
            // recursive
            if (info.fileName()!=".." && info.fileName()!=".")
            {
                size += getSize(sFilePath);
            }
        }
        else
        {
            // Do something with the file here
            return info.size();
        }
    }
    qDebug() << size;
    */
    QFileInfo info(path.toLocalFile());
    return info.size();
}


void Helper::copy(const QString text)
{
    clipBoard->setText(text, QClipboard::Clipboard);
    clipBoard->setText(text, QClipboard::Selection);
}


QString Helper::iconFromSuffix(const QString suffix)
{
    QString image;
    if (suffix == "3dm" || suffix == "max" || suffix == "bmp"
            || suffix == "gif" || suffix == "jpg" || suffix == "png"
            || suffix == "psd" || suffix == "pspimage" || suffix == "thm"
            || suffix == "tif" || suffix == "yuv" || suffix == "ai" || suffix == "drw"
            || suffix == "eps" || suffix == "ps" || suffix == "svg" || suffix == "tiff"){
        image.append("image");
    }
    else if (suffix == "pdf"){
        image.append(suffix);
    }
    else if (suffix == "odp"){
        image.append(suffix);
    }
    else if (suffix == "ods"){
        image.append(suffix);
    }
    else if (suffix == "odt"){
        image.append(suffix);
    }
    else if (suffix == "pps"){
        image.append(suffix);
    }
    else if (suffix == "ppt"){
        image.append(suffix);
    }
    else if (suffix == "xls"){
        image.append(suffix);
    }
    else if (suffix == "txt" || suffix == "doc" || suffix == "docx" || suffix == "log"
             || suffix == "msg" || suffix == "pages" || suffix == "rtf" || suffix == "wpd"
             || suffix == "wps"){
        image.append("txt");
    }
    else if (suffix == "sis" || suffix == "sisx" || suffix == "deb"){
        image.append("sis");
    }
    else{
        image.append("unknown");
    }
    return image;
}
