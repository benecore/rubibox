#include "sortmodel.h"
#include <QDebug>

SortModel *SortModel::_instance = 0;

SortModel::SortModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
    setSortOrder(Qt::DescendingOrder);
    setSortCaseSensitivity(Qt::CaseSensitive);
    connect(this, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(modelReset()), this, SIGNAL(countChanged()));
}


SortModel *SortModel::instance()
{
    if (!_instance)
        _instance = new SortModel;
    return _instance;
}


void SortModel::setSortRole(const QString &role)
{
    invalidateFilter();
    _sortRole = role;
    QSortFilterProxyModel::setSortRole(_roleNames[_sortRole]);
    QSortFilterProxyModel::sort(0, sortOrder());
}


QString SortModel::sortRole() const
{
    return _sortRole;
}


void SortModel::reSort()
{
    setSortRole(_sortRole);
}


QVariant SortModel::get(int index)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    const QModelIndex proxyIndex = this->index(index, 0);
    const QModelIndex sourceIndex = mapToSource(proxyIndex);
    return source->get(sourceIndex.row());
}

ModelItem *SortModel::getItem(int index)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    const QModelIndex proxyIndex = this->index(index, 0);
    const QModelIndex sourceIndex = mapToSource(proxyIndex);
    return source->getItem(sourceIndex.row());
}

void SortModel::appendItem(ModelItem *item)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    source->appendItem(item);
}

void SortModel::insertItem(int index, ModelItem *item)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    const QModelIndex proxyIndex = this->index(index, 0);
    const QModelIndex sourceIndex = mapToSource(proxyIndex);
    source->insertItem(sourceIndex.row(), item);
}


void SortModel::setProperty(const int index, const QVariant &role, const QVariant &value)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    const QModelIndex proxyIndex = this->index(index, 0);
    const QModelIndex sourceIndex = mapToSource(proxyIndex);
    source->setProperty(sourceIndex.row(), role, value);
}


void SortModel::removeItem(int index)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    const QModelIndex proxyIndex = this->index(index, 0);
    const QModelIndex sourceIndex = mapToSource(proxyIndex);
    source->removeItem(sourceIndex.row());
}


bool SortModel::itemExists(const QVariant &id)
{
    Model *source = qobject_cast<Model*>(sourceModel());
    return source->itemExists(id);
}

void SortModel::clear()
{
    Model *source = qobject_cast<Model*>(sourceModel());
    source->clear();
}


void SortModel::syncRoleNames()
{
    setRoleNames(sourceModel()->roleNames());
    QHash<int, QByteArray>::const_iterator i;
    for (i = roleNames().constBegin(); i != roleNames().constEnd(); ++i) {
        _roleNames[i.value()] = i.key();
    }
    qDebug() << "SYNCING ROLES DONE";
}


void SortModel::setModel(QObject *source)
{
    QAbstractItemModel *model = qobject_cast<QAbstractItemModel *>(source);
    if (!model) {
        qDebug() << Q_FUNC_INFO << "error";
        return;
    }

    //connect(model, SIGNAL(modelReset()), this, SLOT(syncRoleNames()));
    QSortFilterProxyModel::setSourceModel(model);
    syncRoleNames();
    setSortRole("isDir");
}


void SortModel::setSortOrder(const Qt::SortOrder order)
{
    QSortFilterProxyModel::sort(0, order);
}
