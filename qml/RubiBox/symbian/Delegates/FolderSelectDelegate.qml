// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../common"


Item{
    id: root
    width: ListView.view.width

    height: nameLabel.height + sizeLabel.height + (platformStyle.paddingMedium*2)


    signal clicked
    signal pressAndHold



    Image {
        id: indicator
        anchors{
            right: parent.right
            verticalCenter: parent.verticalCenter
            margins: platformStyle.paddingMedium
        }
        source: privateStyle.imagePath("qtg_graf_drill_down_indicator", Settings.inverted)
        sourceSize.width: platformStyle.graphicSizeSmall
        sourceSize.height: platformStyle.graphicSizeSmall
    }


    HLine{
        anchors.bottom: parent.bottom
    }


    Image{
        id: thumb
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: platformStyle.paddingMedium
        }

        sourceSize.width: !Settings.nativeIcons ? platformStyle.graphicSizeLarge : 55
        sourceSize.height: !Settings.nativeIcons ? platformStyle.graphicSizeLarge : 55
        width: sourceSize.width
        height: sourceSize.height
        smooth: true
        source: Helper.getIcon(!Settings.inverted, isDir, 2, Settings.nativeIcons, name)
    }


    Label{
        id: nameLabel
        anchors {
            left: thumb.right
            top: parent.top
            right: isDir ? indicator.left : parent.right
            margins: platformStyle.paddingMedium
        }
        opacity: isHidden ? 0.6 : 1
        verticalAlignment: Text.AlignTop
        elide: Text.ElideRight
        font.pixelSize: platformStyle.fontSizeLarge
        text: name
        platformInverted: Settings.inverted
    }



    Label{
        id: sizeLabel
        anchors {
            left: thumb.right
            bottom: parent.bottom
            margins: platformStyle.paddingMedium
        }
        text: isDir ? "folder" : Helper.convSize(size)
        font.pixelSize: platformStyle.fontSizeSmall
        color: selected ? "white" : platformStyle.colorNormalMid
    }



    MouseArea{
        id: mouseArea
        anchors {
            fill: parent
        }
        onPressed: {
            Helper.playEffect(Helper.ThemeBasicItem)
            list.currentIndex = index
        }
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress);
            root.pressAndHold()
        }
    }
}
