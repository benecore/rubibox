import QtQuick 1.1

import "../scripts/const.js" as Const

Rectangle{
    id: root

    //visible: !Settings.sectionSeparator

    property alias lineColor: root.color

    height: 1
    width: parent.width
    color: Settings.colorizeHLine ? Settings.activeColor : Const.COLOR_INVERTED_SECONDARY_FOREGROUND
    anchors.horizontalCenter: parent.horizontalCenter;
}
