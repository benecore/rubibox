// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../Buttons"
import "../../scripts/utils.js" as Utils

ToolBarLayout{
    id: root


    ToolIcon{
        platformIconId: Session.isRoot ? "toolbar-close" : "toolbar-up"
        onClicked: {
            if (Session.isRoot)
                Qt.quit()
            else{
                Session.listFolder(Session.parentFolder)
            }
        }
        onPressAndHold: {
            if (!Session.isRoot){
                Session.listFolder()
            }
        }
    }
    ToolButton{
        onClicked: {
            if (Utils.CM_ITEM_CONFLICT && (Utils.CM_FOLDER_ID !== Session.currentFolder)){
                window.showConflictdialog()
                return;
            }
            if ((Utils.CM_FOLDER_ID !== Session.currentFolder) && !Session.loading){
                Session.copyItem()
            }else{
                infoBanner.parent = window.pageStack
                infoBanner.text = qsTr("Can't copy to same folder")
                infoBanner.show()
            }
        }
        text: qsTr("%1Paste here").arg((Utils.CM_FOLDER_ID !== Session.currentFolder) ? "<font color=\"green\">" : "<font color=\"red\">")
    }
    ToolButton{
        anchors.right: parent.right
        anchors.rightMargin: 5
        text: qsTr("Cancel")
        onClicked: {
            if (!Session.loading){
                Session.action = "normal"
            }
        }
    }

}
