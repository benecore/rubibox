import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../../scripts/const.js" as Const
import "../../common"


Item{
    id: root
    width: root.ListView.view.width
    height: Const.LIST_ITEM_HEIGHT_SMALL

    signal clicked

    BorderImage {
        id: selectedBackground
        anchors.fill: parent
        opacity: mouseArea.pressed ? 1 : 0
        source: theme.inverted ?
                    'image://theme/meegotouch-list-fullwidth-inverted-background-pressed-vertical-center' :
                    'image://theme/meegotouch-list-fullwidth-background-pressed-vertical-center'
    }


    Label{
        id: nameLabel
        anchors{
            left: parent.left
            verticalCenter: parent.verticalCenter
            right: parent.right
            margins: Const.MARGIN_DEFAULT
        }
        elide: Text.ElideRight
        text: name
    }


    MouseArea{
        id: mouseArea
        anchors.fill: parent
        onPressed: {
            listView.currentIndex = index
            Helper.playEffect(Helper.ThemeBasicItem)
        }

        onClicked: root.clicked()
    }

}
