#include <QtGui/QApplication>
#include <QtDeclarative>
#include <QTextCodec>
#include "qmlapplicationviewer.h"

#include "session.h"
#include "factory.h"
#include "filebrowser.h"
#include "filemanager.h"

#ifdef Q_OS_HARMATTAN
#include "shareui.h"
#endif

Q_DECL_EXPORT int main(int argc, char *argv[])
{
#if !defined(AUTH_IS_SET)
#error Set CLIENT_ID and CLIENT_SECRET in the file CONSTANTS_H and uncomment line 8 in the PRO (RubiBox.pro) file
#endif
    QScopedPointer<QApplication> app(createApplication(argc, argv));


    QTextCodec::setCodecForTr(QTextCodec::codecForName("utf-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));


    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);

    /* Expose CLIENT_ID and CLIENT_SECRET to QML */
    viewer.rootContext()->setContextProperty("_CLIENT_ID", CLIENT_ID);
    viewer.rootContext()->setContextProperty("_CLIENT_SECRET", CLIENT_SECRET);

    /*Expose APP VERSION to QML*/
    app->setApplicationVersion(APP_VERSION);
    viewer.rootContext()->setContextProperty("version", app->applicationVersion());

    /* Expose type of build (IS_PRO) to QML */
    viewer.rootContext()->setContextProperty("isPro", false);

    Factory factory;
    viewer.engine()->setNetworkAccessManagerFactory(&factory);

    Session *session = new Session(qApp);
    viewer.rootContext()->setContextProperty("Session", session);
    viewer.rootContext()->setContextProperty("Helper", session->helper);
    viewer.rootContext()->setContextProperty("BModel", session->bModelSort);
    viewer.rootContext()->setContextProperty("TModel", session->tModel);
    viewer.rootContext()->setContextProperty("Box", session->boxApi);
    viewer.rootContext()->setContextProperty("Database", session->db);
    viewer.rootContext()->setContextProperty("Settings", session->settings);

    qmlRegisterType<FileManager>("com.devpda.models", 1, 0, "FileManager");

#ifdef Q_OS_HARMATTAN
    ShareUI shareUI;
    viewer.rootContext()->setContextProperty("shareUI", &shareUI);
#endif

#ifdef Q_OS_SYMBIAN
    viewer.rootContext()->setContextProperty("isSymbian", true);
    //viewer.setMainQmlFile(QLatin1String("qml/RubiBox/symbian/Init.qml"));
    viewer.setSource(QUrl("qrc:/qml/RubiBox/symbian/Init.qml"));
#elif defined(Q_OS_HARMATTAN)
    viewer.rootContext()->setContextProperty("isSymbian", false);
    //viewer.setMainQmlFile(QLatin1String("qml/RubiBox/harmattan/main.qml"));
    viewer.setSource(QUrl("qrc:/qml/RubiBox/harmattan/main.qml"));
#else
    viewer.rootContext()->setContextProperty("isSymbian", true);
    viewer.setMainQmlFile(QLatin1String("qml/RubiBox/symbian/Init.qml"));
#endif
    viewer.showExpanded();

    int result = app->exec();

    delete session;

    return result;
}
