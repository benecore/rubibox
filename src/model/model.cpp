#include "model.h"
#include <QDebug>

Model::Model(ModelItem *prototype, QObject *parent) :
    QAbstractListModel(parent), m_prototype(prototype)
{
    setRoleNames(m_prototype->roleNames());

    connect(this, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(modelReset()),
            this, SIGNAL(countChanged()));
}

Model::~Model()
{
    qDebug() << Q_FUNC_INFO << " destructor";
    delete m_prototype;
}



int Model::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _list.size();
}


QVariant Model::data(const QModelIndex &index, int role) const
{
    if(index.row() < 0 || index.row() >= _list.size())
        return QVariant();
    return _list.at(index.row())->data(role);
}


QModelIndex Model::indexFromItem(ModelItem *item)
{
    Q_ASSERT(item);
    for(int row = 0; row < _list.size(); ++row) {
        if(_list.at(row) == item) return index(row);
    }
    return QModelIndex();
}


void Model::handleDataChanged()
{
    ModelItem *item = static_cast<ModelItem*>(sender());
    QModelIndex index = indexFromItem(item);
    if (index.isValid())
        emit dataChanged(index, index);
}



QVariant Model::get(int index)
{
    if (index < 0 || index >= _list.size()){
        return QVariant();
    }
    ModelItem *item = _list.at(index);
    QVariantMap data;
    QHashIterator<int, QByteArray> hashItr(item->roleNames());
    while(hashItr.hasNext()){
        hashItr.next();
        data.insert(hashItr.value(), item->data(hashItr.key()).toString());
    }
    return QVariant(data);
}


ModelItem *Model::getItem(int index)
{
    if (index < 0 || index >= _list.size()) {
        return 0;
    }
    return _list.at(index);
}


void Model::appendItem(ModelItem *item)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    connect(item, SIGNAL(dataChanged()), SLOT(handleDataChanged()));
    _list.append(item);
    endInsertRows();
}


void Model::appendItems(const QList<ModelItem*> &items)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount()+items.size()-1);
    foreach(ModelItem *item, items) {
        connect(item, SIGNAL(dataChanged()), SLOT(handleDataChanged()));
        _list.append(item);
    }
    endInsertRows();
}


void Model::insertItem(int index, ModelItem *item)
{
    beginInsertRows(QModelIndex(), index, index);
    connect(item, SIGNAL(dataChanged()), SLOT(handleDataChanged()));
    _list.insert(index, item);
    endInsertRows();
}


void Model::setProperty(const int index, const QVariant &role, const QVariant &value)
{
    ModelItem *item = _list.at(index);
    QHashIterator<int, QByteArray> hashItr(item->roleNames());
    while(hashItr.hasNext()){
        hashItr.next();
        if (hashItr.value() == role.toByteArray()){
            item->setProperty(role, value);
            return;
        }
    }
}


void Model::removeItem(int index)
{
    if (index < 0 || index >= _list.size()){
        return;
    }
    beginRemoveRows(QModelIndex(), index, index);
    delete _list.takeAt(index);
    endRemoveRows();
}



bool Model::itemExists(const QVariant &id)
{
    foreach(ModelItem *item, _list){
        if (item->id() == id) return true;
    }
    return false;
}




void Model::clear()
{
    if (_list.size()){
        qDebug() << Q_FUNC_INFO;
        beginRemoveRows(QModelIndex(), 0, rowCount()-1);
        qDeleteAll(_list);
        _list.clear();
        endRemoveRows();
    }
}
