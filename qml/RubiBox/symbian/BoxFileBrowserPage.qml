import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1
import com.devpda.models 1.0

import "Component"
import "../scripts/createobject.js" as ObjectCreator
import "../scripts/utils.js" as Utils
import "Delegates"
import "Buttons"
import "ToolBars"

DevPDA{
    id: root


    function uploadSelected(){
        for (var i = 0; i<fileManager.count; i++){
            if (fileManager.get(i).selected){
                //console.debug("PATH: "+fileManager.get(i).path+"\nSIZE: "+fileManager.get(i).size)
                Session.uploadItem(fileManager.get(i).path, fileManager.get(i).size)
            }
        }
        deselectAllFiles()
        infoBanner.parent = root.pageStack
        infoBanner.text = qsTr("Files added to queue")
        infoBanner.open()
    }


    function selectAllFiles(){
        for (var i = 0; i < fileManager.count; ++i){
            if (!fileManager.get(i).isDir && fileManager.get(i).size <= Settings.maxUpSize){
                fileManager.get(i).selected = true
            }
        }
    }


    function deselectAllFiles(){
        for (var i = 0; i < fileManager.count; ++i){
            fileManager.get(i).selected = false
        }
    }


    function isSelectedFiles(){
        isFilesSelected = false
        for (var i = 0; i < fileManager.count; i++){
            if (fileManager.get(i).selected){
                isFilesSelected = true;
                break;
            }
        }
        console.debug("IS SELECTED: "+isFilesSelected)
        return isFilesSelected;
    }


    function updateUploadSize(){
        Session.userInfo()
    }


    tools: ToolBarLayout{
        ToolIcon{
            iconSource: fileManager.isRoot ? "toolbar-previous" : platformInverted ? "Images/up.png" : "Images/up_white.png"
            onClicked: {
                if (fileManager.isRoot){
                    root.pageStack.pop()
                }else{
                    fileManager.back()
                }
            }
            onPressAndHold: root.pageStack.pop()
        }
        ToolIcon{
            iconSource: "toolbar-home"
            onClicked: fileManager.folder = ""
        }
        ToolIcon{
            iconSource: {
                var icon;
                if (!platformInverted){
                    icon = Settings.multi_selection ? "Images/multisel_white.png" : "Images/singlesel_white.png";
                    return icon;
                }else{
                    icon = Settings.multi_selection ? "Images/multisel.png" : "Images/singlesel.png";
                }
            }
            onClicked: {
                if (Settings.multi_selection){
                    Settings.multi_selection = false;
                    deselectAllFiles()
                }else{
                    Settings.multi_selection = true
                }
            }
        }
        ToolIcon{
            iconSource: platformInverted ? "Images/menu.png" : "Images/menu_white.png"
            onClicked: {
                if (isSelectedFiles()){
                    menuDeselectAll.open()
                }else{
                    menuSelectAll.open()
                }
            }
        }
    }


    Menu{
        id: menuSelectAll
        MenuLayout{
            MenuItem{text: qsTr("Select All"); onClicked: selectAllFiles()}
            MenuItem{text: qsTr("Update upload size"); onClicked: updateUploadSize()}
        }
    }

    Menu{
        id: menuDeselectAll
        MenuLayout{
            MenuItem{text: qsTr("Select None"); onClicked: deselectAllFiles()}
            MenuItem{text: qsTr("Update upload size"); onClicked: updateUploadSize()}
        }
    }


    Menu{
        id: menuSelected
        MenuLayout{
            MenuItem{text: qsTr("Upload selected"); onClicked: uploadSelected()}
        }
    }

    Menu{
        id: menuNoneSelected
        MenuLayout{
            MenuItem{text: qsTr("Upload"); onClicked: uploadSelected()}
        }
    }


    property bool isFilesSelected
    devText: fileManager.folder
    onStatusChanged: {
        if (status === PageStatus.Activating){
            if (Settings.maxUpSize === 0){
                Session.userInfo();
            }
        }
    }


    FileManager{
        id: fileManager
    }


    Label{
        id: noItems
        opacity: fileManager.count ? 0 : 1
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: parent.height/2
        }
        text: qsTr("Empty folder")
        font.pixelSize: 40
        font.italic: true
        color: platformStyle.colorNormalMid

        Behavior on opacity {PropertyAnimation{duration: fileManager.count ? 0 : 50}}
    }

    ListView{
        id: list
        opacity: fileManager.count ? 1 : 0
        cacheBuffer: 1000
        anchors{
            fill: parent
            topMargin: devMargin
        }
        model: fileManager

        delegate: BoxFileBrowserDelegate{
            onClicked: {
                if (isDir){
                    fileManager.folder = path
                }else{
                    if (Settings.multi_selection){
                        if (!(size > Settings.maxUpSize)){
                            if (fileManager.get(index).selected){
                                fileManager.get(index).selected = false;
                            }else{
                                fileManager.get(index).selected = true;
                            }
                        }else{
                            infoBanner.parent = root.pageStack
                            infoBanner.text = qsTr("Big file size for upload")
                            infoBanner.open()
                        }
                    }else{
                        if(!(size > Settings.maxUpSize)){
                            Session.uploadItem(path, size)
                            infoBanner.parent = root.pageStack
                            infoBanner.text = qsTr("%1 added to queue").arg(name)
                            infoBanner.open()
                        }else{
                            infoBanner.parent = root.pageStack
                            infoBanner.text = qsTr("Big file size for upload")
                            infoBanner.open()
                        }
                    }
                }
            }
            onPressAndHold: {
                if (isSelectedFiles() && Settings.multi_selection){
                    menuSelected.open()
                }else{
                    if (!isDir){
                        menuNoneSelected.open()
                    }
                }
            }
        }
    }


    ScrollDecorator{
        flickableItem: list
    }

}
