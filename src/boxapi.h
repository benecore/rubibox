#ifndef BOXAPI_H
#define BOXAPI_H

#include <QObject>
#include <QVariantMap>
#include <QtNetwork>
#include <QXmlStreamReader>
#include "constants.h"
#include "json/json.h"
#include "qupfile.h"
#include "storage/settings.h"
#include "model/items/boxitem.h"
#include "model/items/transferitem.h"
#include "storage/database.h"

#include "model/sortmodel.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif


const QString CRLF("\r\n");
const QString BOUNDARY("--benecore-DevPDA");
const QString SEPARATOR("--" + BOUNDARY + CRLF);
const QString END_MULTI_FORM(CRLF + "--" + BOUNDARY + "--" + CRLF);


class BoxApi : public QObject
{
    Q_OBJECT
public:
    BoxApi(QObject *parent = 0);
    BoxApi(Settings *settings, QObject *parent = 0);
    ~BoxApi();


    enum Action{
        GET_TOKEN,
        REFRESH_TOKEN,
        GET_TICKET,
        LIST_FOLDER,
        CREATE_FOLDER,
        RENAME,
        COPY,
        MOVE,
        REMOVE,
        DOWNLOAD,
        UPLOAD,
        USER_INFO,
        COMMENTS,
        SHARE_LINK,
        SYNC_STATE
    };


signals:
    void requestStart();
    void requestFinished();
    void clear();
    void tokenDone(const QString token, const QString refresh_token);
    void ticketDone(const QString ticket);
    /* - */
    void requestFinishedWithError(int errorCode);
    void header(const QString parent, const QString current);
    void currentFolder(const QVariant currentFolder);
    void parentFolder(const QVariant parentFolder);

    void allItemsLoaded();
    void itemsDone();
    void addItemDone(ModelItem *item);
    void createFolderDone(const int index, ModelItem *item);
    void copyItemDone(const int index, ModelItem *item);
    void copyActionDone();
    void moveItemDone(const int index, ModelItem *item);
    void moveActionDone();
    void renameItemDone(const QVariant role, const QVariant newName);
    void removeItemDone(const int index);
    void syncStateDone(const QVariant role, const QVariant syncState);
    void userInfoDone(const QVariantMap user);

    void transferDone(const int index = 0);
    void allTransfersDone();
    void removeFromQueueDone();
    void uploadItemDone(const QString toFolder, const QVariantMap item);
    void shareLinkDone(const QString shareLink);


    /** este nekontrolovane */
    void insertItemDone(const QVariantMap item);
    void commentsDone(const QVariantMap comment);
    /** refresh token expired **/
    void refreshTokenDone(const QString token, const QString refresh_token);
    void refreshTokenExpired();
    


public slots:
    /** Set token for Auth header **/
    void setToken(QByteArray token);
    /** Set request action for response parsing **/
    void setAction(Action action);
    /** Sets If-Match header when needed **/
    void setIfMatch(const QByteArray etag);
    /** Sets current item index **/
    void setIndex(const int index);
    /** Clear all temporally data **/
    void clearData();



    /** Remove Item from queue **/
    void removeFromQueue(const QVariant &id);



public slots:
    /** getAuthToken **/
    void getAuthToken(const QVariant code);
    /** Refresh token **/
    void renewToken();
    /** Get ticket **/
    void getTicket();
    /** list folder by ID **/
    void listFolder(const QVariant id, const int offset = 0);
    /** Create folder **/
    void createFolder(const QVariant folderName, const QVariant folderId);
    /** Copy item **/
    void copyItem(const bool isFolder, const QVariant itemId/*Item to copy*/, const QVariant destinationId/*Folder where copy item*/, const QVariant newName = QVariant());
    /** Move Item **/
    void moveItem(const bool isFolder, const QVariant itemId, const QVariant destinationId, const QVariant newName = QVariant());
    /** Rename item **/
    void renameItem(const bool isFolder, const QVariant newName, const QVariant id);
    /** Remove item **/
    void removeItem(const bool isFolder, const QVariant id, const QVariant recursive = true);
    /** Sync state **/
    void syncState(const QVariant id, const QVariant sync_state);
    /** User info **/
    void userInfo();
    /** Get comments **/
    void comments(const QVariant id);
    /** Create shre link **/
    void share_link(const bool isFolder, const QVariant id);
    /** add to transfer queue **/
    void addToTransferQueue(ModelItem *item);
    void cancelDownload();
    QVariant getCurrentItemId() const;


private slots:
    /** Sets Auth header for the requests **/
    inline void setHeader(QNetworkRequest &request){
        request.setRawHeader("Authorization", "Bearer "+m_token);
    }
    void finished(QNetworkReply *reply);
    /** parse auth token **/
    void parseToken(QByteArray response);
    /** parse ticket action **/
    void parseTicket(QByteArray response);
    /** Parse create folder **/
    void parseCreateFolder(QByteArray response);
    /** parse list folder action **/
    void parseItems(QByteArray response);
    /** Parse copy item **/
    void parseCopy(QByteArray response);
    /** Parse move item **/
    void parseMove(QByteArray response);
    /** parse rename action **/
    void parseRename(QByteArray response);
    /** parse remove action **/
    void parseRemove(QByteArray response);
    /** parse sync state **/
    void parseSyncState(QByteArray response);
    /** parseUserInfo **/
    void parseUserInfo(QByteArray response);
    /** parseComments **/
    void parseComments(QByteArray response);
    /** parseShareLink **/
    void parseShareLink(QByteArray response);




    /** Ignore SSL Errors ***/
    void sslErrors(QNetworkReply* reply, QList<QSslError> errors);

    void startNext();
    void prepareDownload();
    void startUpload();
    void prepareFinished(QNetworkReply *reply);
    void doDownload(const QUrl url);
    void progress(qint64 bytesReceived, qint64 bytesTotal);
    void ready();
    void finished();
    void finishedUpload();



private:
    QNetworkAccessManager *manager;
    QNetworkAccessManager *file_transfers;
    QUpFile *upFile;
    QNetworkReply *reply;
    Settings *m_settings;
    Action m_action;
    QByteArray m_token,
    m_etag;
    int m_index;
    JsonReader reader;
    TransferItem* m_item;
    QQueue<ModelItem*>m_queue;
    QTime downloadTime;
    QFile output;
    bool m_prepareDownload;

};

#endif // BOXAPI_H
