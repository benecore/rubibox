import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../common"


Item{
    id: root
    width: ListView.view.width
    height: 40

    signal clicked

    HLine{
        anchors{
            bottom: parent.bottom
        }
    }


    Label{
        id: nameLabel
        platformInverted: Settings.inverted
        anchors{
            left: parent.left
            verticalCenter: parent.verticalCenter
            right: parent.right
        }
        elide: Text.ElideRight
        text: name
    }


    MouseArea{
        id: mouseArea
        anchors.fill: parent
        onPressed: {
            listView.currentIndex = index
            Helper.playEffect(Helper.ThemeBasicItem)
        }

        onClicked: root.clicked()
    }

}
