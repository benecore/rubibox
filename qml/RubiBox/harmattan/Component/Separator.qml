import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/const.js" as Const

Item {
    id: root

    property alias text: label.text

    height: 20

    Label {
        id: label
        anchors { right: parent.right; top: parent.top }
        font.pixelSize: Const.FONT_SMALL
        font.bold: true
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        height: 1
        anchors { left: parent.left; right: label.left; rightMargin: 10 ; verticalCenter: label.verticalCenter }
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
    }
}
