// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../../scripts/const.js" as Const
import "../../scripts/utils.js" as Utils
import "../../common"


Item{
    id: root
    width: ListView.view.width


    height: Const.LIST_ITEM_HEIGHT_DEFAULT

    property bool cmCheck: (Session.action !== "normal")

    opacity: (cmCheck && !isDir || cmCheck && Utils.CM_ITEM_ID === id) ? 0.5 : 1
    enabled: opacity == 0.5 ? false : true


    signal clicked
    signal pressAndHold


    MoreIndicator {
         id: indicator
         anchors {
             right: parent.right
             verticalCenter: parent.verticalCenter
         }
         visible: isDir
     }


    HLine{
        anchors.bottom: parent.bottom
    }

    Rectangle{
        anchors.fill: parent
        opacity: selected ? 1 : 0
        color: Settings.activeColor

        Behavior on opacity {PropertyAnimation{duration: selected ? 400 : 50}}
    }

    BorderImage {
        id: selectedBackground
        anchors.fill: parent
        opacity: mouseArea.pressed ? selected ? 0 : 1 : 0
        source: Settings.inverted ?
                    'image://theme/meegotouch-list-fullwidth-inverted-background-pressed-vertical-center' :
                    'image://theme/meegotouch-list-fullwidth-background-pressed-vertical-center'
    }


    Image{
        id: thumb
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: Const.PADDING_MEDIUM
        }

        sourceSize.width: !Settings.nativeIcons ? 85 : 65
        sourceSize.height: !Settings.nativeIcons ? 85 : 65
        width: sourceSize.width
        height: sourceSize.height
        smooth: true
        source: Helper.getIcon(Settings.inverted, isDir, 2, Settings.nativeIcons, name)
        Image{
            visible: sync_state
            anchors{
                right: parent.right
                bottom: parent.bottom
                bottomMargin: !Settings.nativeIcons ? 5 : 0
            }
            source: "../Images/synced.png"
            width: 35
            height: width
        }
    }


    Label{
        id: nameLabel
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? Const.PADDING_MEDIUM : Const.PADDING_XLARGE
            top: parent.top
            right: isDir ? indicator.left : parent.right
            margins: Const.PADDING_MEDIUM
        }
        verticalAlignment: Text.AlignTop
        elide: Text.ElideRight
        font.pixelSize: Const.FONT_LARGE
        text: name
        color: !Settings.inverted ? "black" : "white"
    }


    Label{
        id: sizeLabel
        anchors {
            left: thumb.right
            leftMargin: !Settings.nativeIcons ? Const.PADDING_MEDIUM : Const.PADDING_XLARGE
            bottom: parent.bottom
            margins: Const.PADDING_MEDIUM
        }
        text: qsTr("size: %1").arg(Helper.convSize(size))
        font.pixelSize: Const.FONT_SMALL
        color: selected ? !Settings.inverted ? "black" : "white" : Const.COLOR_INVERTED_SECONDARY_FOREGROUND
    }


    MouseArea{
        id: mouseArea
        anchors {
            fill: parent
        }
        onPressed: {
            Helper.playEffect()
            list.currentIndex = index
            Session.currentIndex = index
        }
        onClicked: root.clicked()
        onPressAndHold: {
            Helper.playEffect(Helper.ThemeLongPress);
            root.pressAndHold()
        }
    }
}
