#ifndef BOXITEM_H
#define BOXITEM_H

#include "model/model.h"
#include <QObject>
#include <QVariantMap>
#include <QVariant>
#include <QStringList>



class BoxItem : public ModelItem
{
public:
    BoxItem(QObject *parent = 0) : ModelItem(parent) {}
    explicit BoxItem(QVariantMap data, QObject *parent = 0);


    enum Roles{
        ID,
        SEQUENCE_ID,
        ETAG,
        SHA1,
        NAME,
        SIZE,
        SELECTED,
        SHARE_LINK,
        IS_DIR,
        SYNCED
    };

/*
    QHash<int, QByteArray>roles;
    roles[IS_DIR] = "isDir";
    roles[SEQUENCE_ID] = "seq_id";
    roles[ETAG] = "etag";
    roles[SHA1] = "sha1";
    roles[NAME] = "name";
    roles[SIZE] = "size";
    roles[SELECTED] = "selected";
    roles[SHARE_LINK] = "share_link";
    setRoleNames(roles);
  */


    QVariant data(int role) const;
    QHash<int, QByteArray> roleNames() const;


public slots:
    inline QVariant id() const { return _data["id"]; }
    inline QVariant seq_id() const { return _data["sequence_id"]; }
    inline QVariant etag() const { return _data["etag"]; }
    inline QVariant sha1() const { return _data["sha1"]; }
    inline QVariant name() const { return _data["name"]; }
    inline QVariant size() const { return _data["size"]; }
    inline QVariant share_link() const { return _data["shared_link"].toMap()["url"]; }
    inline bool selected() const { return _data["selected"].toBool(); }
    inline bool isDir() const { return (_data["type"].toString() == "folder"); }
    inline bool sync_state() const { return (_data["sync_state"].toString() == "synced"); }

    inline void setProperty(const QVariant &role, const QVariant &value);


private:
    QVariantMap _data;

};

#endif // BOXITEM_H
