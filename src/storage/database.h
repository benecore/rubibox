#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql>

#ifdef QT_DEBUG
#include <QDebug>
#endif

class Database : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool authorized READ authorized NOTIFY authorizedChanged)
    Q_PROPERTY(QByteArray token READ getToken NOTIFY tokenChanged)
    Q_PROPERTY(QString active_account READ active_account NOTIFY activeAccountChanged)
    Q_PROPERTY(int accountsCount READ accountsCount NOTIFY accountsCountChanged)
    Q_PROPERTY(QList<QVariant> accounts READ accounts NOTIFY accountsChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(bool usePassword READ usePassword WRITE setUsePassword NOTIFY usePasswordChanged)
public:
    Database(QObject *parent = 0);
    virtual ~Database();
    
    static Database *instance();

signals:
    void accountAdded();
    void authorizedChanged();
    void tokenChanged();
    void activeAccountChanged();
    void accountsCountChanged();
    void accountsChanged();
    void passwordChanged();
    void usePasswordChanged();


public:
    QByteArray getToken();
    QByteArray getRefreshToken();

public slots:
    bool clearAllAccounts();
    bool clearUnusedAccounts();
    bool addAccount(const QVariant &accountName, const int &active = 0);
    bool deleteAccount(const QVariant &accountName);
    bool setToken(const QVariant &accountName, const QVariant &token, const QVariant &refresh_token);
    bool setAsActive(const QVariant &accountName);




private slots:
    void initialize();
    bool authorized();

    QString active_account();
    int accountsCount();
    QList<QVariant> accounts();
    QString password();
    bool usePassword();
    bool setUsePassword(const QVariant &usePassword);
    bool setPassword(const QVariant &pass);




private:
    static Database *_instance;
    QSqlDatabase m_database;
    bool m_authorized;
    
};

#endif // DATABASE_H
