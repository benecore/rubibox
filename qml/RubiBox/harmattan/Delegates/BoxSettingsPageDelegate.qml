// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../../scripts/const.js" as Const
import "../../common"
import "../Component"

Item{
    id: root
    width: ListView.view.width

    height: Const.LIST_ITEM_HEIGHT_DEFAULT


    signal clicked


    HLine{
        visible: true
        anchors.bottom: parent.bottom
    }


    MoreIndicator {
        id: indicator
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
    }


    BorderImage {
        id: selectedBackground
        anchors.fill: parent
        visible: mouseArea.pressed
        source: Settings.inverted ?
                    'image://theme/meegotouch-list-fullwidth-inverted-background-pressed-vertical-center' :
                    'image://theme/meegotouch-list-fullwidth-background-pressed-vertical-center'
    }


    Label{
        anchors {
            fill: parent
            leftMargin: 10
            rightMargin: 60
        }
        verticalAlignment: Text.AlignVCenter
        //font.pixelSize: Const.FONT_LARGE
        text: name
    }


    MouseArea{
        id: mouseArea
        anchors.fill: parent
        onClicked: {
            root.clicked()
        }
        onPressed: {
            list.currentIndex = index
            Helper.playEffect(Helper.ThemeBasicItem)
        }
    }
}
