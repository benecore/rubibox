#ifndef FILEMANAGERITEM_H
#define FILEMANAGERITEM_H

#include <QObject>
#include <QFileInfo>
#include <QDateTime>
#include <QVariant>

class FileManagerItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name NOTIFY dataChanged)
    Q_PROPERTY(QString baseName READ baseName NOTIFY dataChanged)
    Q_PROPERTY(QString path READ path NOTIFY dataChanged)
    Q_PROPERTY(qint64 size READ size NOTIFY dataChanged)
    Q_PROPERTY(QString lastModified READ lastModified NOTIFY dataChanged)
    Q_PROPERTY(QString lastAccessed READ lastAccessed NOTIFY dataChanged)
    Q_PROPERTY(bool isDir READ isDir NOTIFY dataChanged)
    Q_PROPERTY(bool isHidden READ isHidden NOTIFY dataChanged)
    Q_PROPERTY(bool isReadable READ isReadable NOTIFY dataChanged)
    Q_PROPERTY(bool isWritable READ isWritable NOTIFY dataChanged)
    Q_PROPERTY(bool isRoot READ isRoot NOTIFY dataChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY dataChanged)
public:
    FileManagerItem(QFileInfo &info, QObject *parent = 0);


signals:
    void dataChanged();
    

public slots:
    inline QString name() const { return m_info.fileName(); }
    inline QString baseName() const { return m_info.baseName(); }
    inline QString path() const { return m_info.filePath(); }
    inline qint64 size() const { return m_info.size(); }
    inline QString lastModified() const { return m_info.lastModified().toUTC().toString("dd/MM/yyyy | hh:mm:ss"); }
    inline QString lastAccessed() const { return m_info.lastRead().toUTC().toString("dd/MM/yyyy | hh:mm:ss"); }
    inline bool isDir() const { return m_info.isDir(); }
    inline bool isHidden() const { return m_info.isHidden(); }
    inline bool isReadable() const { return m_info.isReadable(); }
    inline bool isWritable() const { return m_info.isWritable(); }
    inline bool isRoot() const { return m_info.isRoot(); }
    inline bool selected() const { return m_selected; }

    inline void setSelected(const QVariant &value){
        m_selected = value.toBool();
        emit dataChanged();
    }


private:
    QFileInfo m_info;
    bool m_selected;
    
};

#endif // FILEMANAGERITEM_H
