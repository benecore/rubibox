// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1


QueryDialog{
    id: queryDialog

    acceptButtonText: qsTr("Remove")
    rejectButtonText: qsTr("Cancel")

    titleText: qsTr("Remove")
    message: qsTr("Are you sure you want to remove <br><font color=\"%1\">%2</font> ?").arg(Settings.activeColor).arg(getCurrentItem().name)

    onRejected: reject()
    onAccepted: {
        Session.removeItem()
    }
}
