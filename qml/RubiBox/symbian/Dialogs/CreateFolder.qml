// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "../../common"
import "../Component"

Sheet{
    id: root



    rejectButtonText: qsTr("Cancel")
    acceptButtonText: qsTr("Create")

    onAccepted: {
        if (root.destroyWhenClosed){
            root.destroy(600);
        }
        if (field.text !== ""){
            Session.createFolder(field.text)
        }else{
            infoBanner.parent = root
            infoBanner.text = qsTr("Folder name can't be empty")
            infoBanner.open()
        }
    }

    onRejected: if (root.destroyWhenClosed) root.destroy(600);


    onStatusChanged: {
        if (status == DialogStatus.Opening){
            field.text = ""
            field.focus = true
        }
    }

    content: Item{
        anchors.fill: parent
        anchors.margins: platformStyle.paddingLarge

        Column{
            width: parent.width
            spacing: 10

            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: platformStyle.fontSizeLarge
                text: qsTr("Create folder")
                platformInverted: Settings.inverted
            }

            HLine{
                width: parent.width
            }

            TextField{
                id: field
                platformInverted: Settings.inverted
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                placeholderText: qsTr("folder name")
                inputMethodHints: Qt.ImhNoPredictiveText
                echoMode: TextInput.Normal
                validator: RegExpValidator{regExp: /[^\\//|:?*<>]*/}
                Keys.onReturnPressed: {
                    if (text !== ""){
                        focus = false;
                        closeSoftwareInputPanel()
                        root.accept()
                    }else{
                        infoBanner.parent = root
                        infoBanner.text = qsTr("Folder name can't be empty")
                        infoBanner.open()
                    }
                }
                onFocusChanged: focus ? platformOpenSoftwareInputPanel() : closeSoftwareInputPanel()
            }
        }
    }
}
