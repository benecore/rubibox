#ifndef FACTORY_H
#define FACTORY_H

#include <QObject>
#include <QtDeclarative>

class Factory : public QObject, public QDeclarativeNetworkAccessManagerFactory
{
    Q_OBJECT
public:
    explicit Factory(QObject *parent = 0);
    virtual QNetworkAccessManager *create(QObject *parent);
    
public slots:
    void ignoreSSLErrors(QNetworkReply* reply,QList<QSslError> errors);


private:
    QNetworkAccessManager* myManager;
    
};

#endif // FACTORY_H
