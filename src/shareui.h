#ifndef SHAREUI_H
#define SHAREUI_H

#include <QObject>
#include <maemo-meegotouch-interfaces/shareuiinterface.h>
#include <MDataUri>


class ShareUI : public QObject
{
    Q_OBJECT
public:
    explicit ShareUI(QObject *parent = 0);
    
signals:
    
public slots:
    void share(const QString &title, const QString &url);
    
};

#endif // SHAREUI_H
