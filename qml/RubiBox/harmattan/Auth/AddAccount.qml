// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import QtWebKit 1.0


import "../Component"

Sheet{
    id: root

    property string accountName
    property alias webViewUrl: webView.url
    property bool isSettings: false

    function checkUrlForToken() {
        if (/code/i.test(webView.url)) {
            var code = webView.url.toString().split("code=")[1]
            console.debug("GRANT TOKEN: "+code)
            if (code){
                Session.getAuthToken(code)
            }
        }
        else{
            console.debug("URL: "+webView.url)
        }
    }


    Connections{
        target: Box
        ignoreUnknownSignals: true
        onTokenDone: {
            console.debug("CALLED FROM AddAccount")
            if (Database.setToken(accountName, token, refresh_token)){
                if (Database.accounts <= 1){
                    window.pageStack.replace(Qt.resolvedUrl("../BoxPage.qml"))
                    Session.listFolder()
                }
                root.accept()
            }
        }
    }


    onStatusChanged: {
        if (status === DialogStatus.Opening){
            webView.urlChanged.connect(checkUrlForToken)
            webView.url = "https://app.box.com/api/oauth2/authorize?client_id=".concat(_CLIENT_ID).concat("&redirect_uri=https://devpda.net/oauth.php&response_type=code&state=security_token")
        }
    }


    rejectButtonText: qsTr("Cancel")
    onRejected: {
        Database.clearUnusedAccounts()
        webView.urlChanged.disconnect(checkUrlForToken)
    }

    onAccepted: webView.urlChanged.disconnect(checkUrlForToken)


    content: Item {
        anchors.fill: parent

        Flickable {
            id: webFlicker


            anchors.fill: parent
            contentWidth: Math.max(webView.width, parent.width);
            contentHeight: Math.max(webView.height, parent.height);
            boundsBehavior: Flickable.DragOverBounds
            clip: true
            visible: webView.url != ""

            WebView {
                id: webView

                preferredWidth: webFlicker.width
                preferredHeight: webFlicker.height
                opacity:(status == WebView.Loading) ? 0 : 1
                onUrlChanged: {
                    if (/code/i.test(url)) {
                        var code = webView.url.toString().split("code=")[1]
                        console.debug("GRANT TOKEN: "+code)
                        if (code){
                            Session.getAuthToken(code)
                        }
                    }
                }

                Behavior on opacity { PropertyAnimation { properties: "opacity"; duration: 300 } }
            }
        }


        ScrollDecorator {
            flickableItem: webFlicker
        }



        BusyDialog {
            id: busyDialog

            anchors.centerIn: parent
            visible: (webView.status === WebView.Loading) && (root.status === DialogStatus.Open)
        }
    }
}
