#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QDesktopServices>

#include "constants.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString activeColor READ activeColor WRITE setActiveColor NOTIFY settingsChanged)
    Q_PROPERTY(QString activeColorString READ activeColorString WRITE setActiveColorString NOTIFY settingsChanged)
    Q_PROPERTY(QString activeColorScheme READ activeColorScheme WRITE setActiveColorScheme NOTIFY settingsChanged)
    Q_PROPERTY(bool inverted READ inverted WRITE setInverted NOTIFY invertedChanged)
    Q_PROPERTY(bool nativeIcons READ nativeIcons WRITE setNativeIcons NOTIFY nativeIconsChanged)
    Q_PROPERTY(bool multi_selection READ multi_selection WRITE setMultiSelection NOTIFY multiSelectionChanged)
    Q_PROPERTY(qint64 maxUpSize READ maxUpSize WRITE setMaxUpSize NOTIFY maxUpSizeChanged)
    Q_PROPERTY(bool colorizeHLine READ colorizeHLine WRITE setColorizeHLine NOTIFY colorizeHLineChanged)
    Q_PROPERTY(int orientation READ orientation WRITE setOrientation NOTIFY orientationChanged)
    Q_PROPERTY(bool sectionSeparator READ sectionSeparator WRITE setSectionSeparator NOTIFY sectionSeparatorChanged)
    Q_PROPERTY(QString downloadFolder READ downloadFolder WRITE setDownloadFolder NOTIFY downloadFolderChanged)
    Q_PROPERTY(int itemsLimit READ itemsLimit WRITE setItemsLimit NOTIFY itemsLimitChanged)
public:
    explicit Settings(QObject *parent = 0);
    ~Settings();
    
signals:
    void settingsChanged();
    void invertedChanged(const bool value);
    void nativeIconsChanged();
    void multiSelectionChanged(const bool value);
    void maxUpSizeChanged();
    void colorizeHLineChanged();
    void orientationChanged(const int orientation /*0 = Auto, 1 = Portrait, 2 = Landscape*/);
    void sectionSeparatorChanged();
    void downloadFolderChanged();
    void itemsLimitChanged();


public slots:
    inline QString activeColor() const { return m_activeColor; }
    inline QString activeColorString() const { return m_activeColorString; }
    inline QString activeColorScheme() const { return m_activeColorScheme; }
    inline bool inverted() const { return m_inverted; }
    inline bool nativeIcons() const { return m_nativeIcons; }
    inline bool multi_selection() const { return m_multiSelection; }
    inline qint64 maxUpSize() const { return m_maxUpSize; }
    inline bool colorizeHLine() const { return m_colorizeHLine; }
    inline int orientation() const { return m_orientation; }
    inline bool sectionSeparator() const { return m_sectionSeparator; }
    inline QString downloadFolder() const { return m_downloadFolder; }
    inline int itemsLimit() const { return m_itemsLimit; }



    /** Sets functions **/
    inline void setActiveColor(const QVariant &value) {
        m_activeColor = value.toString();
        settings->beginGroup(APPEARANCE);
        settings->setValue("activeColor", m_activeColor);
        settings->endGroup();
        emit settingsChanged();
    }
    inline void setActiveColorString(const QVariant &value) {
        m_activeColorString = value.toString();
        settings->beginGroup(APPEARANCE);
        settings->setValue("activeColorString", m_activeColorString);
        settings->endGroup();
        emit settingsChanged();
    }
    inline void setActiveColorScheme(const QVariant &value) {
        m_activeColorScheme = value.toString();
        settings->beginGroup(APPEARANCE);
        settings->setValue("activeColorScheme", m_activeColorScheme);
        settings->endGroup();
        emit settingsChanged();
    }
    inline void setInverted(const QVariant &value) {
        m_inverted = value.toBool();
        settings->beginGroup(APPEARANCE);
        settings->setValue("inverted", m_inverted);
        settings->endGroup();
        emit invertedChanged(m_inverted);
    }
    inline void setNativeIcons(const QVariant &value) {
        m_nativeIcons = value.toBool();
        settings->beginGroup(APPEARANCE);
        settings->setValue("native_icons", m_nativeIcons);
        settings->endGroup();
        emit nativeIconsChanged();
    }
    inline void setMultiSelection(const QVariant &value) {
        m_multiSelection = value.toBool();
        settings->beginGroup(OPTIONS);
        settings->setValue("multi_selection", m_multiSelection);
        settings->endGroup();
        emit multiSelectionChanged(value.toBool());
    }
    inline void setMaxUpSize(const QVariant &value) {
        m_maxUpSize = value.toLongLong();
        settings->beginGroup(BASE);
        settings->setValue("max_up_size", m_maxUpSize);
        settings->endGroup();
        emit maxUpSizeChanged();
    }
    inline void setColorizeHLine(const QVariant &value) {
        m_colorizeHLine = value.toBool();
        settings->beginGroup(APPEARANCE);
        settings->setValue("colorize_h_line", m_colorizeHLine);
        settings->endGroup();
        emit colorizeHLineChanged();
    }
    inline void setOrientation(const QVariant &value) {
        m_orientation = value.toInt();
        settings->beginGroup(BASE);
        settings->setValue("orientation", m_orientation);
        settings->endGroup();
        emit orientationChanged(m_orientation);
    }
    inline void setSectionSeparator(const QVariant &value) {
        m_sectionSeparator = value.toBool();
        settings->beginGroup(BASE);
        settings->setValue("section_separator", m_sectionSeparator);
        settings->endGroup();
        emit sectionSeparatorChanged();
    }
    inline void setDownloadFolder(const QVariant &value) {
        m_downloadFolder = value.toString();
        settings->beginGroup(BASE);
        settings->setValue("download_folder", m_downloadFolder);
        settings->endGroup();
        emit downloadFolderChanged();
    }
    inline void setItemsLimit(const QVariant &value) {
        m_itemsLimit = value.toInt();
        settings->beginGroup(BASE);
        settings->setValue("itemsLimit", m_itemsLimit);
        settings->endGroup();
        emit itemsLimitChanged();
    }
    


private:
    QSettings *settings;

    QString m_activeColor,
    m_activeColorString,
    m_activeColorScheme,
    m_downloadFolder;
    qint64 m_maxUpSize;
    bool m_inverted,
    m_nativeIcons,
    m_multiSelection,
    m_colorizeHLine,
    m_sectionSeparator;
    int m_orientation,
    m_itemsLimit;
};

#endif // SETTINGS_H
